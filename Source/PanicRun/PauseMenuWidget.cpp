﻿#include "PauseMenuWidget.h"

#include "Components/Button.h"
#include "Components/WidgetSwitcher.h"
#include "Kismet/GameplayStatics.h"

void UPauseMenuWidget::NativeConstruct()
{
	Super::NativeConstruct();

	if (_resumeButton != nullptr)
	{
		_resumeButton->OnClicked.AddDynamic(this, &UPauseMenuWidget::Resume);
	}

	if (_settingsButton != nullptr)
	{
		_settingsButton->OnClicked.AddDynamic(this, &UPauseMenuWidget::ShowSettingsWidget);
	}
	
	if (_backButton != nullptr)
	{
		_backButton->SetVisibility(ESlateVisibility::Hidden);
		_backButton->OnClicked.AddDynamic(this, &UPauseMenuWidget::ShowPauseMenuWidget);
	}

	if (_quitToMainMenuButton != nullptr)
	{
		_quitToMainMenuButton->OnClicked.AddDynamic(this, &UPauseMenuWidget::QuitToMainMenu);
	}
}

void UPauseMenuWidget::SetVisibility(ESlateVisibility inVisibility)
{
	Super::SetVisibility(inVisibility);

	APlayerController* playerController = Cast<APlayerController>(GetOwningPlayer());
	if (playerController == nullptr)
	{
		return;
	}

	APawn* playerPawn = playerController->GetPawn();

	if (inVisibility == ESlateVisibility::Visible)
	{
		if (playerPawn != nullptr)
		{
			playerPawn->DisableInput(playerController);
		}
		
		playerController->SetShowMouseCursor(true);
	}
	else
	{
		if (playerPawn != nullptr)
		{
			playerPawn->EnableInput(playerController);
		}
		
		playerController->SetShowMouseCursor(false);
	}
}

void UPauseMenuWidget::Resume()
{
	SetVisibility(ESlateVisibility::Collapsed);
}

void UPauseMenuWidget::ShowSettingsWidget()
{
	if (_widgetSwitcher != nullptr)
	{
		_widgetSwitcher->SetActiveWidgetIndex(1);
	}
}

void UPauseMenuWidget::ShowPauseMenuWidget()
{
	if (_widgetSwitcher != nullptr)
	{
		_widgetSwitcher->SetActiveWidgetIndex(0);
	}
}

void UPauseMenuWidget::QuitToMainMenu()
{
	UGameplayStatics::OpenLevel(this, "MainMenuMap");
}
