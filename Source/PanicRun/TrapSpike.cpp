﻿#include "TrapSpike.h"
#include "RunnerCharacter.h"


ATrapSpike::ATrapSpike()
{
	
}

void ATrapSpike::Interact(APlayerController* player, bool isFake)
{
	Super::Interact(player, isFake);
}

void ATrapSpike::Activate()
{
	if (!HasAuthority())
	{
		return;
	}
	
	//Get overlapping actors and stun all runners
	
	UE_LOG(LogTemp, Log, TEXT("Interact Called in trap"));
	TArray<AActor*> OverlappingActors;
	_killRange->GetOverlappingActors(OverlappingActors);
	for (AActor* Actor : OverlappingActors)
	{
		//UE_LOG(LogTemp, Log, TEXT("Tested overlapping actor"));
		if (ARunnerCharacter* RunnerCharacter = Cast<ARunnerCharacter>(Actor))
		{
			//UE_LOG(LogTemp, Log, TEXT("Found runner"));
			RunnerCharacter->KillPlayer();
		}
	}
}

void ATrapSpike::FakeActivate()
{
	if (!HasAuthority())
	{
		return;
	}
	//unimplemented();
	UE_LOG(LogTemp, Log, TEXT("Fake activation!"));
}

void ATrapSpike::PreInitializeComponents()
{
	Super::PreInitializeComponents();
}

void ATrapSpike::BeginPlay()
{
	Super::BeginPlay();

	//simulate warden interacting with trap every 5 seconds
	//GetWorld()->GetTimerManager().SetTimer(MemberTimerHandle, this, &ATrapSpike::RepeatingFunction, 5.0f, true, 1.0f);
}

//test function for simulating warden
void ATrapSpike::RepeatingFunction()
{
	ATrapSpike::Interact(nullptr, false);
}
