﻿#include "KillVolume.h"

#include "RunnerCharacter.h"
#include "Components/BoxComponent.h"

AKillVolume::AKillVolume()
{
	_boxCollision = CreateDefaultSubobject<UBoxComponent>(TEXT("BoxCollision"));
	RootComponent = _boxCollision;

	_boxCollision->OnComponentBeginOverlap.AddDynamic(this, &AKillVolume::OnOverlapBegin);
}

void AKillVolume::OnOverlapBegin(UPrimitiveComponent* overlappedComponent, AActor* otherActor,
                                 UPrimitiveComponent* otherComp, int otherBodyIndex, bool fromSweep,
                                 const FHitResult& sweepResult)
{
	if (ARunnerCharacter* runner = Cast<ARunnerCharacter>(otherActor))
	{
		runner->KillPlayer();
	}
}
