﻿#include "TrapBase.h"

#include "Animation/AnimInstance.h"
#include "Components/AudioComponent.h"
#include "Components/BoxComponent.h"
#include "Components/SkeletalMeshComponent.h"
#include "UObject/ConstructorHelpers.h"
#include "Runtime/Engine/Classes/Sound/SoundCue.h"

ATrapBase::ATrapBase()
{
	PrimaryActorTick.bCanEverTick = false;
	bReplicates = true;

	_floorMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("FloorMesh"));
	_floorMesh->SetCollisionEnabled(ECollisionEnabled::ProbeOnly);
	_floorMesh->SetGenerateOverlapEvents(false);

	UStaticMesh* CubeMesh = ConstructorHelpers::FObjectFinder<UStaticMesh>(TEXT("StaticMesh'/Engine/BasicShapes/Cube.Cube'")).Object;

	_floorMesh->SetStaticMesh(CubeMesh);
	_floorMesh->SetWorldScale3D({5.0f, 10.0f, 0.5f});

	RootComponent = _floorMesh;
	
	_killRange = CreateDefaultSubobject<UBoxComponent>(TEXT("KillRange"));
	_killRange->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	_killRange->SetupAttachment(_floorMesh);
	
	_trapMesh = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("TrapMesh"));
	_trapMesh->SetCollisionEnabled(ECollisionEnabled::ProbeOnly);
	_trapMesh->SetGenerateOverlapEvents(false);
	_trapMesh->SetupAttachment(_killRange);

	_audioComponent = CreateDefaultSubobject<UAudioComponent>(TEXT("AudioComponent"));
	_audioComponent->SetupAttachment(RootComponent);
}

void ATrapBase::PreInitializeComponents()
{
	if (IsNetMode(NM_DedicatedServer))
	{
		_trapMesh->DestroyComponent();
		_floorMesh->DestroyComponent();
	}
}

void ATrapBase::PostInitializeComponents()
{
	Super::PostInitializeComponents();

	if(_soundOnActivation != nullptr)
	{
		_audioComponent->SetSound(_soundOnActivation);
	}
}

void ATrapBase::NetMulticast_Activation_Implementation()
{
	if (AnimMontage)
	{
		if((AnimInst = _trapMesh->GetAnimInstance()) != nullptr)
		{
			UE_LOG(LogTemp, Log, TEXT("Animation is playing!"));
			if(AnimInst != nullptr)
			{
				AnimInst->Montage_Play(AnimMontage);
			}
			if(_soundOnActivation != nullptr)
			{
				_audioComponent->Play();
			}
		}
	}
}

void ATrapBase::NetMulticast_FakeActivation_Implementation()
{
	if(_soundOnActivation != nullptr)
	{
		_audioComponent->Play();
	}
}

void ATrapBase::ReActivate()
{
	//UE_LOG(LogTemp, Log, TEXT("Trap can be activated again"));
	_isOnCooldown = false;
}

void ATrapBase::Interact(APlayerController* player, bool isFake)
{
	if (!HasAuthority())
	{
		return;
	}

	if (_isOnCooldown)
	{
		UE_LOG(LogTemp, Log, TEXT("Trap is on cooldown"));
		return;
	}

	if(!isFake)
	{
		_isOnCooldown = true;
		GetWorld()->GetTimerManager().SetTimer(_cooldownTimerHandle, this, &ATrapBase::ReActivate, _cooldown, false);
		NetMulticast_Activation();
		Activate();
	}
	else
	{
		_isOnCooldown = true;
		GetWorld()->GetTimerManager().SetTimer(_cooldownTimerHandle, this, &ATrapBase::ReActivate, _fakeCooldown, false);
		NetMulticast_FakeActivation();
		FakeActivate();
	}
}
