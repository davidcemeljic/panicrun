﻿#include "TrapPlatforms.h"


ATrapPlatforms::ATrapPlatforms()
{
	_platform1 = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Platform1Mesh"));
	_platform1->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);
	_platform1->SetGenerateOverlapEvents(false);

	_platform1->SetupAttachment(_floorMesh);

	_platform2 = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Platform2Mesh"));
	_platform2->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);
	_platform2->SetGenerateOverlapEvents(false);

	_platform2->SetupAttachment(_floorMesh);
}

void ATrapPlatforms::Interact(APlayerController* player, bool isFake)
{
	Super::Interact(player, isFake);
}

void ATrapPlatforms::Activate()
{
	if (!HasAuthority())
	{
		return;
	}
}

void ATrapPlatforms::FakeActivate()
{
	if (!HasAuthority())
	{
		return;
	}
}

void ATrapPlatforms::PreInitializeComponents()
{
	Super::PreInitializeComponents();
}

void ATrapPlatforms::BeginPlay()
{
	Super::BeginPlay();
}

void ATrapPlatforms::NetMulticast_Activation_Implementation()
{
	Super::NetMulticast_Activation_Implementation();

	if (_platform1 && _platform2)
	{
		SwapPlatforms();
	}
}

//function that hides the wall
void ATrapPlatforms::SwapPlatforms() const
{
	FVector plat1 = _platform1->GetRelativeLocation();
	plat1.Set(-plat1.X, plat1.Y, plat1.Z);
	_platform1->SetRelativeLocation(plat1);

	FVector plat2 = _platform2->GetRelativeLocation();
	plat2.Set(-plat2.X, plat2.Y, plat2.Z);
	_platform2->SetRelativeLocation(plat2);
}
