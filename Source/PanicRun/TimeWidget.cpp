﻿#include "TimeWidget.h"
#include "Components/TextBlock.h"
#include "Kismet/KismetStringLibrary.h"


void UTimeWidget::SetTime(float minutes)
{
	_time = minutes * 60.0f;
}

void UTimeWidget::NativeTick(const FGeometry& myGeometry, float inDeltaTime)
{
	Super::NativeTick(myGeometry, inDeltaTime);

	if (_time <= 0.0f)
	{
		return;
	}

	_time = FMath::Max(0.0f, _time - inDeltaTime);

	if (_timeText != nullptr)
	{
		_timeText->SetText(FText::FromString(UKismetStringLibrary::TimeSecondsToString(_time)));
	}
}
