// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "InputActionValue.h"
#include "RunnerCharacter.h"
#include "GameFramework/Character.h"
#include "WardenCharacter.generated.h"

class ATrapBase;
class UCameraComponent;
class UInputMappingContext;
class USpringArmComponent;
class UFloatingPawnMovement;

UCLASS(config=Game)
class PANICRUN_API AWardenCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	virtual void PreInitializeComponents() override;
	virtual void PostInitProperties() override;

	virtual void BeginPlay() override;
	virtual void Tick(float deltaSeconds) override;

	virtual void PossessedBy(AController* newController) override;
	virtual void UnPossessed() override;

protected:
	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;
	
private:
	UPROPERTY(EditDefaultsOnly, Category = "Warden|Components")
	USpringArmComponent* _cameraBoom;

	UPROPERTY(EditDefaultsOnly, Category = "Warden|Components")
	UCameraComponent* _followCamera;

	UPROPERTY(EditDefaultsOnly, Category = "Warden|StaticMesh")
	UStaticMeshComponent* _staticMesh;
	
	UPROPERTY(EditDefaultsOnly, Category = "Warden|Input")
	UInputMappingContext* _wardenMappingContext;
	
	UPROPERTY(EditDefaultsOnly, Category = "Warden|Input")
	UInputAction* _moveAction;

	UPROPERTY(EditDefaultsOnly, Category = "Warden|Input")
	UInputAction* _lookAction;

	UPROPERTY(EditDefaultsOnly, Category = "Warden|Input")
	UInputAction* _activateAction;

	UPROPERTY(EditDefaultsOnly, Category = "Warden|Input")
	UInputAction* _fakeActivateAction;

	UPROPERTY(EditDefaultsOnly, Category = "Warden|Gameplay")
	int _startingShopPoints = 3;

	//UPROPERTY(ReplicatedUsing = "OnRep_ShopPoints")
	//int _shopPoints = 3;

	UPROPERTY(EditDefaultsOnly, Category = "Warden|Gameplay")
	float _trapActivationRange = 1750.0f;

	UPROPERTY(EditDefaultsOnly, Category = "Warden|Gameplay")
	float _maxMoveSpeed = 600.0f;
	
public:
	// Sets default values for this character's properties
	AWardenCharacter();

protected:
	
	void Look(const FInputActionValue& value);
	void Move(const FInputActionValue& value);
	
	void FakeActivate(const FInputActionValue& value);
	void Activate(const FInputActionValue& value);

private:
	void BindInput(const AController* newController) const;
	void UnbindInput() const;

	ATrapBase* IsTrapHit() const;
};