﻿#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "WardenWonWidget.generated.h"

class UTextBlock;

UCLASS()
class PANICRUN_API UWardenWonWidget : public UUserWidget
{
	GENERATED_BODY()

private:
	UPROPERTY(meta = (BindWidget))
	UTextBlock* _text;
};
