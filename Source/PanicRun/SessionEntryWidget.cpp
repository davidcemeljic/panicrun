﻿#include "SessionEntryWidget.h"

#include "Components/TextBlock.h"
#include "EOSGameInstance.h"
#include "Components/Border.h"

void USessionEntryWidget::NativeConstruct()
{
	Super::NativeConstruct();
}

void USessionEntryWidget::NativeOnListItemObjectSet(UObject* inObject)
{
	IUserObjectListEntry::NativeOnListItemObjectSet(inObject);

	const UOnlineSessionSearchResultWrapper* session = Cast<UOnlineSessionSearchResultWrapper>(inObject);
	if (session != nullptr)
	{
		if (_sessionNameText != nullptr)
		{
			_sessionNameText->SetText(FText::FromString(session->Session.Session.OwningUserName));
		}

		if (_numPlayersText != nullptr)
		{
			_numPlayersText->SetText(
				FText::FromString(FString::FromInt(session->Session.Session.SessionSettings.NumPublicConnections)));
		}

		if (_pingText != nullptr)
		{
			_pingText->SetText(FText::FromString(FString::FromInt(session->Session.PingInMs)));
		}
	}
}

void USessionEntryWidget::NativeOnItemSelectionChanged(bool isSelected)
{
	IUserObjectListEntry::NativeOnItemSelectionChanged(isSelected);

	if (isSelected)
	{
		_border->SetVisibility(ESlateVisibility::Visible);
	}
	else
	{
		_border->SetVisibility(ESlateVisibility::Hidden);
	}
}
