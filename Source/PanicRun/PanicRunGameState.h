﻿#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameState.h"
#include "PanicRunGameState.generated.h"

DECLARE_EVENT_OneParam(APanicRunGameState, FMatchTimerChanged, float);

DECLARE_EVENT(APanicRunGameState, FMatchTimerExpired);

DECLARE_EVENT(APanicRunGameMode, FWardenWonEvent);

DECLARE_EVENT(APanicRunGameMode, FRunnersWonEvent);

UCLASS()
class PANICRUN_API APanicRunGameState : public AGameState
{
	GENERATED_BODY()

public:
	FMatchTimerChanged OnMatchTimerStarted;
	FMatchTimerExpired OnMatchTimerExpired;

	FWardenWonEvent OnWardenWon;
	FRunnersWonEvent OnRunnersWon;

public:
	/**
	 * Starts the match timer. Should be called on server.
	 * /param[in] matchLength - time left in the match in minutes.
	 */
	void StartMatchTimer(float matchLength);
	
	float GetMatchTimeRemaining() const;

	void RunnersWon();
	void WardenWon();

	// AGameStateBase
protected:
	virtual void EndPlay(const EEndPlayReason::Type endPlayReason) override;

	// AActor
protected:
	virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;

private:
	FTimerHandle _matchTimer;

	UPROPERTY(ReplicatedUsing = OnRep_MatchStartTime)
	FDateTime _matchStartTime;

	/**
	 * Full match duration in minutes.
	 */
	UPROPERTY(Replicated)
	float _matchLength;

private:
	UFUNCTION(NetMulticast, Reliable)
	void NetMulticast_RunnersWon();

	UFUNCTION(NetMulticast, Reliable)
	void NetMulticast_WardenWon();

	UFUNCTION()
	void ClearTimers() const;

	UFUNCTION()
	void MatchTimerExpired() const;

	void SetMatchStartTime(const FDateTime& matchStartTime);
	void OnMatchStartTimeChanged();

	UFUNCTION()
	void OnRep_MatchStartTime();
};
