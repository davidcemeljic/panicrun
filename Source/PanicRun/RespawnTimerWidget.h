﻿#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "RespawnTimerWidget.generated.h"

class UTextBlock;

UCLASS()
class PANICRUN_API URespawnTimerWidget : public UUserWidget
{
	GENERATED_BODY()

public:
	void StartCountdown(float seconds);

protected:
	virtual void NativeTick(const FGeometry& myGeometry, float inDeltaTime) override;
	
private:
	UPROPERTY(meta = (BindWidget))
	UTextBlock* _respawnTimerTextBlock;

	float _time;
};
