﻿#pragma once

#include "CoreMinimal.h"
#include "GameFramework/HUD.h"
#include "PanicRunHUD.generated.h"

class UPauseMenuWidget;
class UWardenWonWidget;
class URunnersWonWidget;
class URespawnTimerWidget;
class UTimeWidget;

UCLASS()
class PANICRUN_API APanicRunHUD : public AHUD
{
	GENERATED_BODY()

public:
	APanicRunHUD();

	void StartMatchTimer(float matchLength) const;
	
	void ShowRespawnWidget(float respawnTime) const;
	
	void TogglePauseMenu() const;

protected:
	virtual void BeginPlay() override;

private:
	UPROPERTY(EditDefaultsOnly, Category = "UI")
	TSubclassOf<UTimeWidget> _timeWidgetClass;

	UPROPERTY(EditDefaultsOnly, Category = "UI")
	TSubclassOf<URespawnTimerWidget> _respawnTimerWidgetClass;

	UPROPERTY(EditDefaultsOnly, Category = "UI")
	TSubclassOf<URunnersWonWidget> _runnersWonWidgetClass;

	UPROPERTY(EditDefaultsOnly, Category = "UI")
	TSubclassOf<UWardenWonWidget> _wardenWonWidgetClass;

	UPROPERTY(EditDefaultsOnly, Category = "UI")
	TSubclassOf<UPauseMenuWidget> _pauseMenuWidgetClass;

	UPROPERTY()
	UTimeWidget* _timeWidget;

	UPROPERTY()
	URespawnTimerWidget* _respawnTimerWidget;
	
	UPROPERTY()
	URunnersWonWidget* _runnersWonWidget;

	UPROPERTY()
	UWardenWonWidget* _wardenWonWidget;

	UPROPERTY()
	UPauseMenuWidget* _pauseMenuWidget;

private:
	
	void ShowRunnersWonWidget();
	void ShowWardenWonWidget();
};
