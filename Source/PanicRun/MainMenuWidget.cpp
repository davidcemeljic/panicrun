﻿#include "MainMenuWidget.h"

#include "SessionBrowserWidget.h"
#include "Components/Button.h"
#include "Components/WidgetSwitcher.h"

void UMainMenuWidget::NativeConstruct()
{
	Super::NativeConstruct();

	if (_playButton != nullptr)
	{
		_playButton->OnClicked.AddDynamic(this, &UMainMenuWidget::ShowSessionBrowser);
	}

	if (_backButton != nullptr)
	{
		_backButton->SetVisibility(ESlateVisibility::Collapsed);
		_backButton->OnClicked.AddDynamic(this, &UMainMenuWidget::ShowMainMenu);
	}

	if (_settingsButton != nullptr)
	{
		_settingsButton->OnClicked.AddDynamic(this, &UMainMenuWidget::ShowSettings);
	}

	if (_quitButton != nullptr)
	{
		_quitButton->OnClicked.AddDynamic(this, &UMainMenuWidget::QuitGame);
	}
}

void UMainMenuWidget::ShowSessionBrowser()
{
	if (_widgetSwitcher != nullptr)
	{
		_widgetSwitcher->SetActiveWidgetIndex(1);

		if (USessionBrowserWidget* browser = Cast<USessionBrowserWidget>(_widgetSwitcher->GetActiveWidget()))
		{
			browser->Refresh();
		}
	}

	if (_backButton != nullptr)
	{
		_backButton->SetVisibility(ESlateVisibility::Visible);
	}
}

void UMainMenuWidget::ShowSettings()
{
	if (_widgetSwitcher != nullptr)
	{
		_widgetSwitcher->SetActiveWidgetIndex(2);
	}

	if (_backButton != nullptr)
	{
		_backButton->SetVisibility(ESlateVisibility::Visible);
	}
}

void UMainMenuWidget::ShowMainMenu()
{
	if (_widgetSwitcher != nullptr)
	{
		_widgetSwitcher->SetActiveWidgetIndex(0);
	}

	if (_backButton != nullptr)
	{
		_backButton->SetVisibility(ESlateVisibility::Collapsed);
	}
}

void UMainMenuWidget::QuitGame()
{
	FGenericPlatformMisc::RequestExit(false);
}
