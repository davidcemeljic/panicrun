#pragma once
#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "InputActionValue.h"
#include "PhysicsControlLimbData.h"
#include "RunnerCharacter.generated.h"

class UInputAction;
class UInputMappingContext;
class UCameraComponent;
class USpringArmComponent;
class UPhysicsControlComponent;

UCLASS(config=Game)
class ARunnerCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	ARunnerCharacter();

	USpringArmComponent* GetCameraBoom() const
	{
		return _cameraBoom;
	}

	UCameraComponent* GetFollowCamera() const
	{
		return _followCamera;
	}

	/*
	 * Called by a trap that needs to instantly kill the player.
	 * Respawns a player at the start after a few seconds.
	 * Called only on server.
	 */
	void KillPlayer();

	/**
	 *	Turn on ragdoll for the player for the given duration.
	 *	Does not necessarily kill the player, just takes away controls and makes physics control the character.
	 *	After stunDuration, the player will get up and be immune to stun for _stunImmunityTimer.
	 *	Called only on server.
	 */
	void StunPlayer(float stunDuration);

	virtual void Reset() override;

	// APawn
public:
	virtual void PreInitializeComponents() override;
	virtual void PostInitializeComponents() override;

	virtual void BeginPlay() override;
	virtual void Tick(float deltaSeconds) override;

	virtual void PossessedBy(AController* newController) override;
	virtual void UnPossessed() override;

	virtual void EndPlay(const EEndPlayReason::Type EndPlayReason) override;


	// APawn
protected:
	virtual void SetupPlayerInputComponent(UInputComponent* PlayerInputComponent) override;

protected:
	void Move(const FInputActionValue& value);
	void Look(const FInputActionValue& Value);

private:
	UPROPERTY(EditDefaultsOnly, Category = "Components")
	USpringArmComponent* _cameraBoom;

	UPROPERTY(EditDefaultsOnly, Category = "Components")
	UCameraComponent* _followCamera;

	UPROPERTY(EditAnywhere, Category = "Runner|Input")
	UInputMappingContext* _defaultMappingContext;

	UPROPERTY(EditDefaultsOnly, Category = "Runner|Input")
	UInputAction* _jumpAction;

	UPROPERTY(EditDefaultsOnly, Category = "Runner|Input")
	UInputAction* _moveAction;

	UPROPERTY(EditDefaultsOnly, Category = "Runner|Input")
	UInputAction* _lookAction;

	UPROPERTY(EditDefaultsOnly, Category = "Runner|Components")
	UPhysicsControlComponent* _physicsControl;

	UPROPERTY(EditDefaultsOnly, Category = "Runner|Gameplay")
	float _defaultStunDuration = 3.0f;

	UPROPERTY(EditDefaultsOnly, Category = "Runner|Gameplay")
	float _stunImmunityDuration = 3.0f;

	bool _isStunned = false;
	bool _isImmuneToStun = false;
	bool _isAwaitingRespawn = false;

	FTimerHandle _stunTimer;
	FTimerHandle _stunImmunityTimer;
	FTimerHandle _respawnTimer;

	UPROPERTY(EditDefaultsOnly, Category = "Runner|PhysicsControls")
	TArray<FPhysicsControlLimbSetupData> _limbSetupData;

	FPhysicsControlNameArray _skAllControls;
	FPhysicsControlNameArray _aaAllControls;
	FPhysicsControlNameArray _allBodyModifiers;

	TMap<FName, FPhysicsControlNameArray> _skLimbControls;
	TMap<FName, FPhysicsControlNameArray> _aaLimbControls;
	TMap<FName, FPhysicsControlNameArray> _limbBodyModifiers;


private:
	void BindInput(const AController* newController) const;
	void UnbindInput() const;

	void SetupPhysicsControls();

	UFUNCTION(NetMulticast, Reliable)
	void NetMulticast_Ragdoll();
	UFUNCTION(NetMulticast, Reliable)
	void NetMulticast_RecoverFromRagdoll();
	UFUNCTION(NetMulticast, Reliable)
	void NetMulticast_RemoveStunImmunity();

	// todo remove
	UFUNCTION()
	void OnCapsuleBeginOverlap(UPrimitiveComponent* overlappedComponent, AActor* otherActor,
	                           UPrimitiveComponent* otherComp, int otherBodyIndex, bool bFromSweep,
	                           const FHitResult& hitResult);
};
