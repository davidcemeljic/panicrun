﻿#pragma once

#include "CoreMinimal.h"
#include "Interactable.h"
#include "GameFramework/Actor.h"
#include "Components/BoxComponent.h"

#include "TrapBase.generated.h"

class UBoxComponent;
class USoundCue;
class UAnimInstance;
class UAnimMontage;

UCLASS(Abstract)
class PANICRUN_API ATrapBase : public AActor, public IInteractable
{
	GENERATED_BODY()

public:

	ATrapBase();
	
	//implemented in base class
	UFUNCTION(BlueprintCallable)
	virtual void Interact(APlayerController* player, bool isFake) override;

	virtual void PreInitializeComponents() override;

	virtual void PostInitializeComponents() override;

	//implemented in child class
	virtual void Activate() PURE_VIRTUAL(ATrapBase::Activate, );

	virtual void FakeActivate() PURE_VIRTUAL(ATrapBase::FakeActivate, );


	
protected:

	/* Handle to manage the timer */
	FTimerHandle _cooldownTimerHandle;
	
	//properties for handling cooldowns
	UPROPERTY(EditDefaultsOnly, Category = "Trap|Gameplay")
	float _cooldown = 5.0f;

	UPROPERTY(EditDefaultsOnly, Category = "Trap|Gameplay")
	float _fakeCooldown = 5.0f;

	UPROPERTY(EditDefaultsOnly, Category = "Trap|Gameplay")
	bool _isOnCooldown = false;
	
	//meshes and components
	UPROPERTY(EditDefaultsOnly, Category = "Trap|Gameplay")
	USkeletalMeshComponent* _trapMesh;

	UPROPERTY(EditDefaultsOnly, Category = "Trap|Gameplay")
	UStaticMeshComponent* _floorMesh;

	UPROPERTY(EditDefaultsOnly, Category = "Trap|Gameplay")
	UBoxComponent* _killRange;
	
	UPROPERTY(EditDefaultsOnly, Category = "Trap|Gameplay")
	UAnimInstance* AnimInst;

	UPROPERTY(EditDefaultsOnly, Category = "Trap|Gameplay")
	USoundCue* _soundOnActivation;
	
	UAudioComponent* _audioComponent;
	
	UPROPERTY(EditDefaultsOnly, Category= "Trap|Gameplay")
	UAnimMontage* AnimMontage;

protected:

	//implement animation here
	UFUNCTION(NetMulticast, Reliable)
	virtual void NetMulticast_Activation();
	
	UFUNCTION(NetMulticast, Reliable)
	virtual void NetMulticast_FakeActivation();

	//make trap activatable again
	void ReActivate();
	
};
