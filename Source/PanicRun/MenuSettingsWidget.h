// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Blueprint/UserWidget.h"

#include "MenuSettingsWidget.generated.h"

class UButton;
/**
 * 
 */
UCLASS()
class PANICRUN_API UMenuSettingsWidget : public UUserWidget
{
	GENERATED_BODY()

public:
	void NativeConstruct() override;

private:
	UPROPERTY(Meta = (BindWidget))
	UButton* _lowGraphicsButton;

	UPROPERTY(Meta = (BindWidget))
	UButton* _mediumGraphicsButton;

	UPROPERTY(Meta = (BindWidget))
	UButton* _highGraphicsButton;

	UPROPERTY(Meta = (BindWidget))
	UButton* _enableLumenButton;

	UPROPERTY()
	UGameUserSettings* _gameUserSettings;

private:
	UFUNCTION()
	void SetGraphicsToLow();

	UFUNCTION()
	void SetGraphicsToMedium();

	UFUNCTION()
	void SetGraphicsToHigh();

	UFUNCTION()
	void SetLumenOption();

	UGameUserSettings* GetGameUserSettings();
};
