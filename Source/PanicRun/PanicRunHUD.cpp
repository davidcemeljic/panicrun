﻿#include "PanicRunHUD.h"

#include "PanicRunGameState.h"
#include "PauseMenuWidget.h"
#include "RespawnTimerWidget.h"
#include "RunnersWonWidget.h"
#include "TimeWidget.h"
#include "WardenWonWidget.h"
#include "Kismet/GameplayStatics.h"

APanicRunHUD::APanicRunHUD() {}

void APanicRunHUD::StartMatchTimer(float matchLength) const
{
	if (_timeWidget != nullptr)
	{
		_timeWidget->SetTime(matchLength);

		if (IsNetMode(NM_ListenServer))
		{
			UE_LOG(LogTemp, Warning, TEXT("Server: Time remaining: %f"), matchLength);
		}
		else if (IsNetMode(NM_Client))
		{
			UE_LOG(LogTemp, Warning, TEXT("Client: Time remaining: %f"), matchLength);
		}
	}
}

void APanicRunHUD::ShowRespawnWidget(float respawnTime) const
{
	if (_respawnTimerWidget != nullptr)
	{
		_respawnTimerWidget->StartCountdown(respawnTime);
	}
}

void APanicRunHUD::TogglePauseMenu() const
{
	if (_pauseMenuWidget == nullptr)
	{
		return;
	}

	if (_pauseMenuWidget->GetVisibility() == ESlateVisibility::Visible)
	{
		_pauseMenuWidget->SetVisibility(ESlateVisibility::Collapsed);
	}
	else
	{
		_pauseMenuWidget->SetVisibility(ESlateVisibility::Visible);
	}
}

void APanicRunHUD::BeginPlay()
{
	Super::BeginPlay();

	if (APanicRunGameState* gameState = Cast<APanicRunGameState>(UGameplayStatics::GetGameState(this)))
	{
		StartMatchTimer(gameState->GetMatchTimeRemaining());

		gameState->OnMatchTimerStarted.AddUObject(this, &APanicRunHUD::StartMatchTimer);
		gameState->OnRunnersWon.AddUObject(this, &APanicRunHUD::ShowRunnersWonWidget);
		gameState->OnWardenWon.AddUObject(this, &APanicRunHUD::ShowWardenWonWidget);
	}

	if (_timeWidgetClass != nullptr && _timeWidget == nullptr)
	{
		_timeWidget = CreateWidget<UTimeWidget>(GetOwningPlayerController(), _timeWidgetClass);
		_timeWidget->AddToPlayerScreen();
	}

	if (_respawnTimerWidgetClass != nullptr)
	{
		_respawnTimerWidget = CreateWidget<URespawnTimerWidget>(GetOwningPlayerController(), _respawnTimerWidgetClass);
		_respawnTimerWidget->SetVisibility(ESlateVisibility::Collapsed);
		_respawnTimerWidget->AddToPlayerScreen();
	}

	if (_pauseMenuWidgetClass != nullptr)
	{
		_pauseMenuWidget = CreateWidget<UPauseMenuWidget>(GetOwningPlayerController(), _pauseMenuWidgetClass);
		_pauseMenuWidget->SetVisibility(ESlateVisibility::Collapsed);
		_pauseMenuWidget->AddToPlayerScreen();
	}
}

void APanicRunHUD::ShowRunnersWonWidget()
{
	if (_runnersWonWidgetClass != nullptr)
	{
		_runnersWonWidget = CreateWidget<URunnersWonWidget>(GetOwningPlayerController(), _runnersWonWidgetClass);
		_runnersWonWidget->AddToPlayerScreen();
	}
}

void APanicRunHUD::ShowWardenWonWidget()
{
	if (_wardenWonWidgetClass != nullptr)
	{
		_wardenWonWidget = CreateWidget<UWardenWonWidget>(GetOwningPlayerController(), _wardenWonWidgetClass);
		_wardenWonWidget->AddToPlayerScreen();
	}
}
