// Copyright Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class PanicRun : ModuleRules
{
	public PanicRun(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

		PublicDependencyModuleNames.AddRange(new string[]
		{
			"Core",
			"CoreUObject",
			"Engine",
			"InputCore",
			"HeadMountedDisplay",
			"UMG",
			"EnhancedInput",
			"PhysicsControl", 
			"OnlineSubsystem",
			"OnlineSubsystemEOS"
		});
	}
}