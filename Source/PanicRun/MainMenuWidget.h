﻿#pragma once
#include "Blueprint/UserWidget.h"

#include "MainMenuWidget.generated.h"

class USessionBrowserWidget;
class UWidgetSwitcher;
class UButton;
UCLASS()
class PANICRUN_API UMainMenuWidget : public UUserWidget
{
	GENERATED_BODY()

public:
	virtual void NativeConstruct() override;

private:
	UPROPERTY(Meta = (BindWidget))
	UButton* _playButton;

	UPROPERTY(Meta = (BindWidget))
	UButton* _backButton;

	UPROPERTY(Meta = (BindWidget))
	UButton* _settingsButton;

	UPROPERTY(Meta = (BindWidget))
	UButton* _quitButton;

	UPROPERTY(Meta = (BindWidget))
	UWidgetSwitcher* _widgetSwitcher;

	// UPROPERTY(EditDefaultsOnly, Category = "WidgetSwitcher")
	// TSubclassOf<USettingsWidget> _settingsWidgetClass;
	
	// UPROPERTY()
	// USettingsWidget* _settingsWidget;

private:
	UFUNCTION()
	void ShowSessionBrowser();

	UFUNCTION()
	void ShowSettings();

	UFUNCTION()
	void ShowMainMenu();

	UFUNCTION()
	void QuitGame();
};
