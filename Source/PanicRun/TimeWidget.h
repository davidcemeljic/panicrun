﻿#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "TimeWidget.generated.h"

class UTextBlock;

UCLASS()
class PANICRUN_API UTimeWidget : public UUserWidget
{
	GENERATED_BODY()
	
public:
	void SetTime(float minutes);
	
	virtual void NativeTick(const FGeometry& myGeometry, float inDeltaTime) override;

private:
	UPROPERTY(meta = (BindWidget))
	UTextBlock* _timeText;

	float _time;
};
