// Fill out your copyright notice in the Description page of Project Settings.


#include "WardenCharacter.h"

#include "EnhancedInputComponent.h"
#include "EnhancedInputSubsystems.h"
#include "TrapBase.h"
#include "Camera/CameraComponent.h"
#include "Components/CapsuleComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/SpringArmComponent.h"
#include "GameFramework/FloatingPawnMovement.h"
#include "GameFramework/PawnMovementComponent.h"
#include "Kismet/KismetMathLibrary.h"
#include "APanicRunPlayerController.h"

// Sets default values
AWardenCharacter::AWardenCharacter()
{
	GetCapsuleComponent()->InitCapsuleSize(20.0f, 20.0f);	

	_cameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	_cameraBoom->SetupAttachment(RootComponent);
	_cameraBoom->TargetArmLength = 350.0f;
	_cameraBoom->bUsePawnControlRotation = true;

	_followCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("Camera"));
	_followCamera->SetupAttachment(_cameraBoom, USpringArmComponent::SocketName);
	_followCamera->bUsePawnControlRotation = false;

	_staticMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Static Mesh"));
	_staticMesh->SetupAttachment(RootComponent);
	_staticMesh->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	_staticMesh->CanCharacterStepUpOn = ECB_No;
	_staticMesh->SetGenerateOverlapEvents(false);

	GetCharacterMovement()->DefaultLandMovementMode = EMovementMode::MOVE_Flying;
	
}

void AWardenCharacter::PreInitializeComponents()
{
	Super::PreInitializeComponents();

	if (IsNetMode(NM_DedicatedServer))
	{
		_followCamera->DestroyComponent();
		_cameraBoom->DestroyComponent();
	}
}

void AWardenCharacter::PostInitProperties()
{
	Super::PostInitProperties();

	//_shopPoints = _startingShopPoints;
}

void AWardenCharacter::BindInput(const AController* newController) const
{
	if (!IsLocallyControlled())
	{
		return;
	}

	if (const APlayerController* playerController = Cast<APlayerController>(newController))
	{
		if (UEnhancedInputLocalPlayerSubsystem* subsystem = ULocalPlayer::GetSubsystem<
			UEnhancedInputLocalPlayerSubsystem>(playerController->GetLocalPlayer()))
		{
			subsystem->AddMappingContext(_wardenMappingContext, 0);
		}
	}
}

void AWardenCharacter::UnbindInput() const
{
	if (const APlayerController* playerController = Cast<APlayerController>(GetController()))
	{
		if (UEnhancedInputLocalPlayerSubsystem* subsystem = ULocalPlayer::GetSubsystem<
			UEnhancedInputLocalPlayerSubsystem>(playerController->GetLocalPlayer()))
		{
			subsystem->RemoveMappingContext(_wardenMappingContext);
		}
	}
}

// Called when the game starts or when spawned
void AWardenCharacter::BeginPlay()
{
	Super::BeginPlay();
	
	GetCharacterMovement()->MaxFlySpeed = _maxMoveSpeed;
	GetCharacterMovement()->MaxAcceleration = 1550.0f;
	GetCharacterMovement()->BrakingDecelerationFlying = 700.0f;
	
	BindInput(GetController());
}

void AWardenCharacter::Look(const FInputActionValue& Value)
{
	const FVector2D LookAxisVector = Value.Get<FVector2D>();

	if (Controller != nullptr)
	{
		// add yaw and pitch input to controller
		AddControllerYawInput(LookAxisVector.X);
		AddControllerPitchInput(LookAxisVector.Y);
	}
}

void AWardenCharacter::Move(const FInputActionValue& value)
{
	const FVector MovementVector = value.Get<FVector>();    // FVector a ne FInputActionValue::Axis3D

	if (Controller != nullptr)
	{
		// find out which way is forward
		const FRotator Rotation = Controller->GetControlRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);

		// get forward vector
		const FVector ForwardDirection = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::X);

		// get right vector 
		const FVector RightDirection = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::Y);

		const FVector UpDirection = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::Z);

		// add movement 
		AddMovementInput(ForwardDirection, MovementVector.Y);
		AddMovementInput(RightDirection, MovementVector.X);
		AddMovementInput(UpDirection, MovementVector.Z);
	}
}

void AWardenCharacter::FakeActivate(const FInputActionValue& value)
{
	ATrapBase* Trap = IsTrapHit();

	UE_LOG(LogTemp, Log, TEXT("Fake"));
	if(Trap == nullptr)
	{
		return;
	}
	
	if(APanicRunPlayerController* PlayerController = Cast<APanicRunPlayerController>(GetController()))
	{
		PlayerController->Interact(Trap, PlayerController, true);
	}
}

void AWardenCharacter::Activate(const FInputActionValue& value)
{
	ATrapBase* Trap = IsTrapHit();

	UE_LOG(LogTemp, Log, TEXT("Activate"));
	if(Trap == nullptr)
	{
		return;
	}
	if(APanicRunPlayerController* PlayerController = Cast<APanicRunPlayerController>(GetController()))
	{
		PlayerController->Interact(Trap, PlayerController, false);
	}
}

// Called every frame
void AWardenCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AWardenCharacter::PossessedBy(AController* newController)
{
	Super::PossessedBy(newController);

	BindInput(newController);
}

void AWardenCharacter::UnPossessed()
{
	Super::UnPossessed();

	UnbindInput();
}

// Called to bind functionality to input
void AWardenCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	if(UEnhancedInputComponent* EIC = CastChecked<UEnhancedInputComponent>(PlayerInputComponent))
	{
		EIC->BindAction(_lookAction, ETriggerEvent::Triggered, this, &AWardenCharacter::Look);
		EIC->BindAction(_moveAction, ETriggerEvent::Triggered, this, &AWardenCharacter::Move);
		EIC->BindAction(_fakeActivateAction, ETriggerEvent::Started, this, &AWardenCharacter::FakeActivate);
		EIC->BindAction(_activateAction, ETriggerEvent::Started, this, &AWardenCharacter::Activate);
	}
}

// Function used for detecting whether the Warden is targeting a trap
// Make sure that QueryOnly (or higher) is selected for the object static mesh collision settings
ATrapBase* AWardenCharacter::IsTrapHit() const
{
	const FVector StartVector = _followCamera->GetComponentLocation();
	const FVector EndVector = StartVector + _trapActivationRange * _followCamera->GetForwardVector();
	
	FHitResult OutHit;

	FCollisionQueryParams Params;

	Params.AddIgnoredActor(this);

	bool HitAnything = GetWorld()->LineTraceSingleByChannel(OutHit, StartVector, EndVector, ECC_Visibility, Params);//ActorLineTraceSingle(HitResult, StartVector, EndVector, ECollisionChannel::ECC_Camera, Params);

	// UE_LOG(LogTemp, Log, TEXT("HitResult %s"), *FString(OutHit.ToString()));
	
	if(!HitAnything)
	{
		return nullptr;
	}

	AActor* HitActor = OutHit.GetActor();
	
	if(ATrapBase* Trap = Cast<ATrapBase>(HitActor))
	{
		return Trap;
	}
	else
	{
		return nullptr;
	}
}

