﻿#include "RespawnTimerWidget.h"
#include "Components/TextBlock.h"

void URespawnTimerWidget::StartCountdown(float seconds)
{
	_time = seconds;
}

void URespawnTimerWidget::NativeTick(const FGeometry& myGeometry, float inDeltaTime)
{
	Super::NativeTick(myGeometry, inDeltaTime);

	if (_time <= 0.0f)
	{
		return;
	}

	_time = FMath::Max(0.0f, _time - inDeltaTime);

	if (_respawnTimerTextBlock != nullptr)
	{
		const FString timeString = FString::Printf(TEXT("RESPAWNING IN %f SECONDS"), _time);
		_respawnTimerTextBlock->SetText(FText::FromString(timeString));
	}

	if (_time <= 0.0f)
	{
		SetVisibility(ESlateVisibility::Collapsed);
	}
}
