﻿#pragma once
#include "TrapBase.h"
#include "TrapFire.generated.h"

UCLASS()

class ATrapFire : public ATrapBase
{
	GENERATED_BODY()
public:
	ATrapFire();
	
	virtual void Interact(APlayerController* player, bool isFake) override;

	virtual void Activate() override;

	virtual void FakeActivate() override;

	/*
 * Called before initialization of components, destroys components that are not needed on the server.
 */
	virtual void PreInitializeComponents() override;

	// AActor

	protected:
	virtual void BeginPlay() override;

protected:
	virtual void NetMulticast_Activation_Implementation() override;
	
private:
	bool _isActive = false;
	
	/* Handle to manage the timer */
	FTimerHandle _PStimerHandle;

	UPROPERTY(EditDefaultsOnly, Category = "Trap|Gameplay")
	UParticleSystem* _PSfire;

	UPROPERTY()
	UParticleSystemComponent* _PSystem;

	void DestroyPS();

	UFUNCTION()
	void OnCollision(UPrimitiveComponent* OverlappedComponent, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

};
