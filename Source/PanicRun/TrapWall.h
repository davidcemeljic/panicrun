﻿#pragma once
#include "TrapBase.h"
#include "TrapWall.generated.h"

UCLASS()

class ATrapWall : public ATrapBase
{
	GENERATED_BODY()
public:
	ATrapWall();
	
	virtual void Interact(APlayerController* player, bool isFake) override;

	virtual void Activate() override;

	virtual void FakeActivate() override;

	/*
 * Called before initialization of components, destroys components that are not needed on the server.
 */
	virtual void PreInitializeComponents() override;


	protected:
	virtual void BeginPlay() override;

protected:
	UPROPERTY(EditDefaultsOnly, Category = "Trap|Gameplay")
	float _wallDuration = 3.0f;
	
	virtual void NetMulticast_Activation_Implementation() override;

	UPROPERTY(EditDefaultsOnly, Category = "Trap|Gameplay")
	UStaticMeshComponent* _wallMesh;
	
private:

	void HideWall() const;

	/* Handle to manage the timer */
	FTimerHandle _wallTimerHandle;

};
