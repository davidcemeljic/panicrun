﻿#include "TrapWall.h"

#include "RunnerCharacter.h"


ATrapWall::ATrapWall()
{
	_wallMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("WallMesh"));
	_wallMesh->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	_wallMesh->SetVisibility(false);
	_wallMesh->SetGenerateOverlapEvents(false);

	_wallMesh->SetupAttachment(_floorMesh);
}

void ATrapWall::Interact(APlayerController* player, bool isFake)
{
	Super::Interact(player, isFake);
}

void ATrapWall::Activate()
{
	if (!HasAuthority())
	{
		return;
	}
}

void ATrapWall::FakeActivate()
{
	if (!HasAuthority())
	{
		return;
	}
}

void ATrapWall::PreInitializeComponents()
{
	Super::PreInitializeComponents();
}

void ATrapWall::BeginPlay()
{
	Super::BeginPlay();
}

void ATrapWall::NetMulticast_Activation_Implementation()
{
	Super::NetMulticast_Activation_Implementation();

	if (_wallMesh != nullptr)
	{
		_wallMesh->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);
		_wallMesh->SetVisibility(true);
		GetWorld()->GetTimerManager().SetTimer(_wallTimerHandle, this, &ATrapWall::HideWall, _wallDuration, false);
	}
}

//function that hides the wall
void ATrapWall::HideWall() const
{
	if (_wallMesh != nullptr)
	{
		_wallMesh->SetCollisionEnabled(ECollisionEnabled::NoCollision);
		_wallMesh->SetVisibility(false);
	}
}
