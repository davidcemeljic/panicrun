﻿#include "EOSGameInstance.h"
#include "Interfaces/OnlineSessionInterface.h"
#include "Interfaces/OnlineIdentityInterface.h"
#include "OnlineSessionSettings.h"
#include "OnlineSubsystem.h"
#include "Kismet/GameplayStatics.h"

UEOSGameInstance::UEOSGameInstance() {}

void UEOSGameInstance::Login()
{
	if (OnlineSubsystem != nullptr)
	{
		if (const IOnlineIdentityPtr identity = OnlineSubsystem->GetIdentityInterface())
		{
			identity->OnLoginCompleteDelegates->AddUObject(this, &UEOSGameInstance::OnLoginComplete);
			
			FOnlineAccountCredentials credentials;
			credentials.Id = FString();
			credentials.Token = FString();
			credentials.Type = FString("accountportal");

			identity->Login(0, credentials);
		}
	}
}

void UEOSGameInstance::CreateSession()
{
	if (isLoggedIn && OnlineSubsystem != nullptr)
	{
		if (const IOnlineSessionPtr sessionPtr = OnlineSubsystem->GetSessionInterface())
		{
			FOnlineSessionSettings sessionSettings;
			sessionSettings.bIsDedicated = false;
			sessionSettings.bIsLANMatch = false;
			sessionSettings.bShouldAdvertise = true;
			sessionSettings.NumPublicConnections = 10;
			sessionSettings.bAllowJoinInProgress = false;
			sessionSettings.bAllowJoinViaPresence = true;
			sessionSettings.bUsesPresence = true;
			sessionSettings.bUseLobbiesIfAvailable = true;
			sessionSettings.Set(SEARCH_KEYWORDS, FString("PanicRun"),
			                    EOnlineDataAdvertisementType::ViaOnlineService);

			sessionPtr->OnCreateSessionCompleteDelegates.AddUObject(this, &UEOSGameInstance::OnCreateSessionComplete);
			sessionPtr->CreateSession(0, _sessionName, sessionSettings);
		}
	}
}

void UEOSGameInstance::DestroySession()
{
	if (isLoggedIn && OnlineSubsystem != nullptr)
	{
		if (const IOnlineSessionPtr sessionPtr = OnlineSubsystem->GetSessionInterface())
		{
			sessionPtr->OnDestroySessionCompleteDelegates.AddUObject(this, &UEOSGameInstance::OnDestroySessionComplete);
			sessionPtr->DestroySession(_sessionName);
		}
	}
}

void UEOSGameInstance::FindSessions()
{
	if (isLoggedIn && OnlineSubsystem != nullptr)
	{
		if (const IOnlineSessionPtr sessionPtr = OnlineSubsystem->GetSessionInterface())
		{
			_sessionSearchSettings = MakeShareable(new FOnlineSessionSearch());
			_sessionSearchSettings->MaxSearchResults = 15;
			_sessionSearchSettings->QuerySettings.Set(SEARCH_KEYWORDS, FString("PanicRun"),
			                                          EOnlineComparisonOp::Equals);
			_sessionSearchSettings->QuerySettings.Set(SEARCH_LOBBIES, true,
			                                          EOnlineComparisonOp::Equals);

			sessionPtr->OnFindSessionsCompleteDelegates.AddUObject(this, &UEOSGameInstance::OnFindSessionsComplete);
			sessionPtr->FindSessions(0, _sessionSearchSettings.ToSharedRef());
		}
	}
}

bool UEOSGameInstance::JoinSession(ULocalPlayer* LocalPlayer, const FOnlineSessionSearchResult& searchResult)
{
	if (isLoggedIn && OnlineSubsystem != nullptr)
	{
		if (const IOnlineSessionPtr sessionPtr = OnlineSubsystem->GetSessionInterface())
		{
			sessionPtr->OnJoinSessionCompleteDelegates.AddUObject(this, &UEOSGameInstance::OnJoinSessionComplete);
			return sessionPtr->JoinSession(0, _sessionName, searchResult);
		}
	}

	return Super::JoinSession(LocalPlayer, searchResult);
}

const TArray<FOnlineSessionSearchResult>& UEOSGameInstance::GetSessions() const
{
	return _sessionSearchSettings->SearchResults;
}

void UEOSGameInstance::Init()
{
	Super::Init();

	OnlineSubsystem = IOnlineSubsystem::Get();
	Login();
}

void UEOSGameInstance::OnLoginComplete(int32 localUserNumber, bool wasSuccessful, const FUniqueNetId& uniqueNetId,
                                       const FString& error)
{
	UE_LOG(LogTemp, Warning, TEXT("Login: %d, error: %s"), wasSuccessful, *error);


	isLoggedIn = wasSuccessful;

	if (error == "Already logged in")
	{
		isLoggedIn = true;
	}

	if (OnlineSubsystem != nullptr)
	{
		if (const IOnlineIdentityPtr identity = OnlineSubsystem->GetIdentityInterface())
		{
			identity->ClearOnLoginCompleteDelegates(0, this);
		}
	}
}

void UEOSGameInstance::OnCreateSessionComplete(FName sessionName, bool wasSuccessful)
{
	UE_LOG(LogTemp, Warning, TEXT("Sesion creation success: %d"), wasSuccessful);

	if (OnlineSubsystem != nullptr)
	{
		if (const IOnlineSessionPtr sessionPtr = OnlineSubsystem->GetSessionInterface())
		{
			sessionPtr->ClearOnCreateSessionCompleteDelegates(this);
		}
	}

	if (wasSuccessful)
	{
		UGameplayStatics::OpenLevel(this, "PanicRunProcedural", true, "listen");
	}
}

void UEOSGameInstance::OnDestroySessionComplete(FName sessionName, bool bWasSuccessful)
{
	UE_LOG(LogTemp, Warning, TEXT("Sesion destroy success: %d"), bWasSuccessful);

	if (OnlineSubsystem != nullptr)
	{
		if (const IOnlineSessionPtr sessionPtr = OnlineSubsystem->GetSessionInterface())
		{
			sessionPtr->ClearOnDestroySessionCompleteDelegates(this);
		}
	}
}

void UEOSGameInstance::OnFindSessionsComplete(bool wasSuccessful)
{
	UE_LOG(LogTemp, Warning, TEXT("Find sessions success: %d"), wasSuccessful);
	if (wasSuccessful)
	{
		UE_LOG(LogTemp, Warning, TEXT("Sesions found: %d"), _sessionSearchSettings->SearchResults.Num());
		OnSessionSearchComplete.Broadcast();
	}

	if (OnlineSubsystem != nullptr)
	{
		if (const IOnlineSessionPtr sessionPtr = OnlineSubsystem->GetSessionInterface())
		{
			sessionPtr->ClearOnFindSessionsCompleteDelegates(this);
		}
	}
}

void UEOSGameInstance::OnJoinSessionComplete(FName sessionName, EOnJoinSessionCompleteResult::Type result)
{
	if (OnlineSubsystem != nullptr && result == EOnJoinSessionCompleteResult::Success)
	{
		if (const IOnlineSessionPtr sessionPtr = OnlineSubsystem->GetSessionInterface())
		{
			sessionPtr->ClearOnJoinSessionCompleteDelegates(this);

			FString connectionInfo;
			if (sessionPtr->GetResolvedConnectString(sessionName, connectionInfo))
			{
				if (!connectionInfo.IsEmpty())
				{
					UE_LOG(LogTemp, Warning, TEXT("Connection info: %s"), *connectionInfo);
					APlayerController* playerController = GetFirstLocalPlayerController();
					if (playerController != nullptr)
					{
						playerController->ClientTravel(connectionInfo, TRAVEL_Absolute);
					}
				}
			}
		}
	}
}
