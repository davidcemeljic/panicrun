﻿#include "InteractableTest.h"

#include "Components/SphereComponent.h"

AInteractableTest::AInteractableTest() {
	PrimaryActorTick.bCanEverTick = true;

	_sphereCollision = CreateDefaultSubobject<USphereComponent>(TEXT("SphereCollision"));
	RootComponent = _sphereCollision;

	_mesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Mesh"));
	_mesh->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	_mesh->SetGenerateOverlapEvents(false);
	_mesh->SetupAttachment(RootComponent);
}

void AInteractableTest::PreInitializeComponents() {
	Super::PreInitializeComponents();
	if (IsNetMode(NM_DedicatedServer)) { _mesh->DestroyComponent(); }
}

void AInteractableTest::BeginPlay() { Super::BeginPlay(); }

void AInteractableTest::Interact(APlayerController* player, bool isFake) {
	UE_LOG(LogTemp, Warning, TEXT("Interacting with InteractableTest"));
}
