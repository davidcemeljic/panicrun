﻿#include "APanicRunPlayerController.h"

#include "EnhancedInputComponent.h"
#include "EnhancedInputSubsystems.h"
#include "Interactable.h"
#include "PanicRunHUD.h"

APanicRunPlayerController::APanicRunPlayerController() {}

void APanicRunPlayerController::RespawnTimerStarted(float respawnTime)
{
	Client_RespawnTimerStarted(respawnTime);
}

void APanicRunPlayerController::Interact(UObject* interactable, APlayerController* playerController, bool isFake)
{
	Server_Interact(interactable, playerController, isFake);
}

void APanicRunPlayerController::SetupInputComponent()
{
	Super::SetupInputComponent();

	if (UEnhancedInputComponent* enhancedInputComponent = Cast<UEnhancedInputComponent>(InputComponent))
	{
		if (UEnhancedInputLocalPlayerSubsystem* subsystem = ULocalPlayer::GetSubsystem<
			UEnhancedInputLocalPlayerSubsystem>(GetLocalPlayer()))
		{
			subsystem->AddMappingContext(_inputMappingContext, 0);
		}

		enhancedInputComponent->BindAction(_openPauseMenuAction, ETriggerEvent::Triggered, this,
		                                   &APanicRunPlayerController::TogglePauseMenu);
	}
}

void APanicRunPlayerController::TogglePauseMenu(const FInputActionValue& inputActionValue)
{
	if (const APanicRunHUD* panicRunHUD = Cast<APanicRunHUD>(GetHUD()))
	{
		panicRunHUD->TogglePauseMenu();
	}
}

void APanicRunPlayerController::Server_Interact_Implementation(UObject* interactable,
                                                               APlayerController* playerController, bool isFake)
{
	if (IInteractable* Trap = Cast<IInteractable>(interactable))
	{
		Trap->Interact(playerController, isFake);
	}
}

void APanicRunPlayerController::Client_RespawnTimerStarted_Implementation(float respawnTime)
{
	if (const APanicRunHUD* panicRunHUD = Cast<APanicRunHUD>(GetHUD()))
	{
		panicRunHUD->ShowRespawnWidget(respawnTime);
	}
}
