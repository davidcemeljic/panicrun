﻿#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "Interactable.generated.h"

UINTERFACE()
class UInteractable : public UInterface {
	GENERATED_BODY()
};

/**
 * Interface that represents an object that can be interacted with.
 */
class PANICRUN_API IInteractable {
	GENERATED_BODY()

public:
	virtual void Interact(APlayerController* player, bool isFake) = 0;
};
