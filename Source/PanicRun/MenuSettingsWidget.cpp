// Fill out your copyright notice in the Description page of Project Settings.


#include "MenuSettingsWidget.h"

#include "Components/Button.h"
#include "GameFramework/GameUserSettings.h"

void UMenuSettingsWidget::NativeConstruct()
{
	Super::NativeConstruct();

	if(_lowGraphicsButton != nullptr)
	{
		_lowGraphicsButton->OnClicked.AddDynamic(this, &UMenuSettingsWidget::SetGraphicsToLow);
	}

	if(_mediumGraphicsButton != nullptr)
	{
		_mediumGraphicsButton->OnClicked.AddDynamic(this, &UMenuSettingsWidget::SetGraphicsToMedium);
	}

	if(_highGraphicsButton != nullptr)
	{
		_highGraphicsButton->OnClicked.AddDynamic(this, &UMenuSettingsWidget::SetGraphicsToHigh);
	}

	if(_enableLumenButton != nullptr)
	{
		_enableLumenButton->OnClicked.AddDynamic(this, &UMenuSettingsWidget::SetLumenOption);
	}

	_gameUserSettings = GetGameUserSettings();

	_gameUserSettings->SetGlobalIlluminationQuality(2);
}

void UMenuSettingsWidget::SetGraphicsToLow()
{
	UE_LOG(LogTemp, Log, TEXT("LOW"));
	_gameUserSettings->SetOverallScalabilityLevel(1);
	_gameUserSettings->SetTextureQuality(0);
	_gameUserSettings->ApplySettings(true);
}

void UMenuSettingsWidget::SetGraphicsToMedium()
{
	UE_LOG(LogTemp, Log, TEXT("MEDIUM"));
	_gameUserSettings->SetOverallScalabilityLevel(2);
	_gameUserSettings->SetTextureQuality(1);
	_gameUserSettings->ApplySettings(true);
}

void UMenuSettingsWidget::SetGraphicsToHigh()
{
	UE_LOG(LogTemp, Log, TEXT("HIGH"));
	_gameUserSettings->SetOverallScalabilityLevel(3);
	_gameUserSettings->SetTextureQuality(3);
	_gameUserSettings->ApplySettings(true);
	
}

void UMenuSettingsWidget::SetLumenOption()
{
	UE_LOG(LogTemp, Log, TEXT("Lumen"));
	
	int CurrentIlluminationQuality = _gameUserSettings->GetGlobalIlluminationQuality();

	if(CurrentIlluminationQuality == 2)
	{
		_gameUserSettings->SetGlobalIlluminationQuality(4);
	}
	else
	{
		_gameUserSettings->SetGlobalIlluminationQuality(2);
	}
	
	_gameUserSettings->ApplySettings(true);
}

UGameUserSettings* UMenuSettingsWidget::GetGameUserSettings()
{
	if(GEngine != nullptr)
	{
		return GEngine->GetGameUserSettings();
	}

	return nullptr;
}
