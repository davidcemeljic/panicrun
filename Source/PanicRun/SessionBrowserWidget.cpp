﻿#include "SessionBrowserWidget.h"

#include "EOSGameInstance.h"
#include "SessionEntryWidget.h"
#include "Components/Button.h"
#include "Components/ListView.h"

void USessionBrowserWidget::NativeConstruct()
{
	Super::NativeConstruct();

	if (_createButton != nullptr)
	{
		_createButton->OnClicked.AddDynamic(this, &USessionBrowserWidget::Create);
	}

	if (_refreshButton != nullptr)
	{
		_refreshButton->OnClicked.AddDynamic(this, &USessionBrowserWidget::Refresh);
	}

	if (_joinButton != nullptr)
	{
		_joinButton->OnClicked.AddDynamic(this, &USessionBrowserWidget::Join);
	}

	if (UEOSGameInstance* gameInstance = GetGameInstance<UEOSGameInstance>())
	{
		gameInstance->OnSessionSearchComplete.AddUObject(this, &USessionBrowserWidget::UpdateSessionList);
	}
}

void USessionBrowserWidget::Create()
{
	if (UEOSGameInstance* gameInstance = GetGameInstance<UEOSGameInstance>())
	{
		if (_createButton != nullptr)
		{
			_createButton->SetIsEnabled(false);
		}

		gameInstance->CreateSession();
	}
}

void USessionBrowserWidget::Refresh()
{
	if (_sessionList == nullptr)
	{
		return;
	}

	_sessionList->ClearListItems();

	if (UEOSGameInstance* gameInstance = GetGameInstance<UEOSGameInstance>())
	{
		gameInstance->FindSessions();
	}
}

void USessionBrowserWidget::Join()
{
	if (_sessionList == nullptr)
	{
		return;
	}
	UEOSGameInstance* gameInstance = GetGameInstance<UEOSGameInstance>();
	if (gameInstance == nullptr)
	{
		return;
	}

	const APlayerController* playerController = GetOwningPlayer();
	if (playerController == nullptr)
	{
		return;
	}

	if (const UOnlineSessionSearchResultWrapper* sessionInfo = _sessionList->GetSelectedItem<UOnlineSessionSearchResultWrapper>())
	{
		const FOnlineSessionSearchResult& session = sessionInfo->Session;
		ULocalPlayer* localPlayer = playerController->GetLocalPlayer();
		if (localPlayer == nullptr)
		{
			UE_LOG(LogTemp, Error, TEXT("Failed to join session %s because"), *session.GetSessionIdStr());
			return;
		}
		
		if (gameInstance->JoinSession(localPlayer, session))
		{
			UE_LOG(LogTemp, Log, TEXT("Joining session %s"), *session.GetSessionIdStr());
		}
		else
		{
			UE_LOG(LogTemp, Error, TEXT("Failed to join session %s"), *session.GetSessionIdStr());
		}
	}
}

void USessionBrowserWidget::UpdateSessionList() const
{
	const UEOSGameInstance* gameInstance = GetGameInstance<UEOSGameInstance>();
	if (gameInstance == nullptr)
	{
		return;
	}

	if (_sessionList != nullptr)
	{
		TArray<UOnlineSessionSearchResultWrapper*> sessions;
		for (const FOnlineSessionSearchResult& session : gameInstance->GetSessions())
		{
			UOnlineSessionSearchResultWrapper* sessionWrapper = NewObject<UOnlineSessionSearchResultWrapper>();
			sessionWrapper->Session = session;
			sessions.Add(sessionWrapper);
		}

		_sessionList->SetListItems(sessions);
	}
}
