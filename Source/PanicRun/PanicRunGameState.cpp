﻿#include "PanicRunGameState.h"
#include "Net/UnrealNetwork.h"

void APanicRunGameState::StartMatchTimer(float matchLength)
{
	_matchLength = matchLength;
	SetMatchStartTime(FDateTime::UtcNow());

	GetWorldTimerManager().SetTimer(_matchTimer, this, &APanicRunGameState::MatchTimerExpired, _matchLength * 60.0f,
	                                false);
}

float APanicRunGameState::GetMatchTimeRemaining() const
{
	return _matchLength * 60.0f - (FDateTime::UtcNow() - _matchStartTime).GetTotalSeconds();
}

void APanicRunGameState::RunnersWon()
{
	NetMulticast_RunnersWon();
}

void APanicRunGameState::WardenWon()
{
	NetMulticast_WardenWon();
}

void APanicRunGameState::EndPlay(const EEndPlayReason::Type endPlayReason)
{
	ClearTimers();

	Super::EndPlay(endPlayReason);
}

void APanicRunGameState::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);
	DOREPLIFETIME(APanicRunGameState, _matchStartTime);
	DOREPLIFETIME(APanicRunGameState, _matchLength);
}

void APanicRunGameState::NetMulticast_RunnersWon_Implementation()
{
	ClearTimers();
	OnRunnersWon.Broadcast();
}

void APanicRunGameState::NetMulticast_WardenWon_Implementation()
{
	ClearTimers();
	OnWardenWon.Broadcast();
}

void APanicRunGameState::ClearTimers() const
{
	GetWorldTimerManager().ClearAllTimersForObject(this);
}

void APanicRunGameState::MatchTimerExpired() const
{
	OnMatchTimerExpired.Broadcast();
}

void APanicRunGameState::SetMatchStartTime(const FDateTime& matchStartTime)
{
	if (matchStartTime != _matchStartTime)
	{
		_matchStartTime = matchStartTime;
		OnMatchStartTimeChanged();
	}
}

void APanicRunGameState::OnMatchStartTimeChanged()
{
	OnMatchTimerStarted.Broadcast(_matchLength);
}

void APanicRunGameState::OnRep_MatchStartTime()
{
	OnMatchStartTimeChanged();
}
