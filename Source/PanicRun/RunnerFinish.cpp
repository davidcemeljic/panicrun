﻿#include "RunnerFinish.h"

#include "PanicRunGameMode.h"
#include "Components/BoxComponent.h"

ARunnerFinish::ARunnerFinish()
{
	bReplicates = true;

	_root = CreateDefaultSubobject<USceneComponent>(TEXT("Root"));
	RootComponent = _root;

	_mesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Mesh"));
	_mesh->SetupAttachment(_root);

	_boxCollision = CreateDefaultSubobject<UBoxComponent>(TEXT("BoxCollision"));
	_boxCollision->SetupAttachment(_root);

	_boxCollision->OnComponentBeginOverlap.AddDynamic(this, &ARunnerFinish::OnBeginOverlap);
}

void ARunnerFinish::OnBeginOverlap(UPrimitiveComponent* overlappedComponent, AActor* otherActor,
                                   UPrimitiveComponent* otherComp, int otherBodyIndex, bool fromSweep,
                                   const FHitResult& sweepResult)
{
	if (HasAuthority())
	{
		if (APanicRunGameMode* gameMode = Cast<APanicRunGameMode>(GetWorld()->GetAuthGameMode()))
		{
			gameMode->RunnerReachedFinish();
		}
	}
}
