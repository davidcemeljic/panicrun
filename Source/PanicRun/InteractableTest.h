﻿#pragma once

#include "CoreMinimal.h"
#include "Interactable.h"
#include "GameFramework/Actor.h"
#include "InteractableTest.generated.h"

class USphereComponent;

UCLASS()
class PANICRUN_API AInteractableTest : public AActor, public IInteractable {
	GENERATED_BODY()

public:
	AInteractableTest();

	// AActor
public:
	/*
	 * Called before initialization of components, destroys components that are not needed on the server.
	 */
	virtual void PreInitializeComponents() override;
	
	// AActor
protected:
	virtual void BeginPlay() override;

	// IInteractable
public:
	virtual void Interact(APlayerController* player, bool isFake) override;

private:
	UPROPERTY(EditDefaultsOnly)
	USphereComponent* _sphereCollision;

	UPROPERTY(EditDefaultsOnly)
	UStaticMeshComponent* _mesh;
};
