﻿#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "RunnersWonWidget.generated.h"

class UTextBlock;

UCLASS()
class PANICRUN_API URunnersWonWidget : public UUserWidget
{
	GENERATED_BODY()

private:
	UPROPERTY(meta = (BindWidget))
	UTextBlock* _textBlock;
};
