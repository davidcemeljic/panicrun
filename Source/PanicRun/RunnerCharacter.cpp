#include "RunnerCharacter.h"
#include "Camera/CameraComponent.h"
#include "Components/CapsuleComponent.h"
#include "Components/InputComponent.h"
#include "EnhancedInputComponent.h"
#include "EnhancedInputSubsystems.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/Controller.h"
#include "GameFramework/SpringArmComponent.h"
#include "PanicRunGameMode.h"
#include "PanicRunGameState.h"
#include "Kismet/GameplayStatics.h"
#include "PhysicsControl/Public/PhysicsControlComponent.h"

ARunnerCharacter::ARunnerCharacter()
{
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);

	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	GetCharacterMovement()->bOrientRotationToMovement = true;
	GetCharacterMovement()->RotationRate = FRotator(0.0f, 500.0f, 0.0f);
	GetCharacterMovement()->JumpZVelocity = 700.f;
	GetCharacterMovement()->AirControl = 0.35f;
	GetCharacterMovement()->MaxWalkSpeed = 500.f;
	GetCharacterMovement()->MinAnalogWalkSpeed = 20.f;
	GetCharacterMovement()->BrakingDecelerationWalking = 2000.f;

	_cameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	_cameraBoom->SetupAttachment(RootComponent);
	_cameraBoom->TargetArmLength = 400.0f;
	_cameraBoom->bUsePawnControlRotation = true;

	_followCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("FollowCamera"));
	_followCamera->SetupAttachment(_cameraBoom, USpringArmComponent::SocketName);
	_followCamera->bUsePawnControlRotation = false;

	_physicsControl = CreateDefaultSubobject<UPhysicsControlComponent>(TEXT("PhysicsControlComponent"));

	GetMesh()->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);
	GetMesh()->SetCollisionProfileName(FName("RunnerMeshPreset"));
	GetMesh()->CanCharacterStepUpOn = ECB_No;
	GetMesh()->SetGenerateOverlapEvents(false);
	GetMesh()->PhysicsTransformUpdateMode = EPhysicsTransformUpdateMode::ComponentTransformIsKinematic;

	GetCapsuleComponent()->SetCollisionProfileName(FName("RunnerCapsule"));
	GetCapsuleComponent()->CanCharacterStepUpOn = ECB_No;
	GetCapsuleComponent()->SetGenerateOverlapEvents(true);
	GetCapsuleComponent()->OnComponentBeginOverlap.AddDynamic(this, &ARunnerCharacter::OnCapsuleBeginOverlap);
}

void ARunnerCharacter::KillPlayer()
{
	if (!HasAuthority())
	{
		return;
	}

	if (_isAwaitingRespawn)
	{
		return;
	}


	UE_LOG(LogTemp, Log, TEXT("Killed player"));
	StunPlayer(-1.0f);


	if (APanicRunGameMode* gameMode = Cast<APanicRunGameMode>(UGameplayStatics::GetGameMode(this)))
	{
		if (APanicRunPlayerController* playerController = Cast<APanicRunPlayerController>(GetController()))
		{
			gameMode->PlayerDied(playerController);
			_isAwaitingRespawn = true;
		}
	}
}

void ARunnerCharacter::StunPlayer(float stunDuration)
{
	if (!HasAuthority())
	{
		return;
	}

	if (_isStunned || _isImmuneToStun || _isAwaitingRespawn)
	{
		return;
	}

	_isStunned = true;
	_isImmuneToStun = true;

	UE_LOG(LogTemp, Log, TEXT("Stunned player for %f seconds"), stunDuration);

	NetMulticast_Ragdoll();
	if (stunDuration > 0.0f)
	{
		GetWorldTimerManager().SetTimer(_stunTimer, this,
		                                &ARunnerCharacter::NetMulticast_RecoverFromRagdoll,
		                                stunDuration, false);
	}
}

void ARunnerCharacter::Reset()
{
	NetMulticast_RecoverFromRagdoll();
	_isStunned = false;
	_isImmuneToStun = false;
	_isAwaitingRespawn = false;
}

void ARunnerCharacter::PreInitializeComponents()
{
	Super::PreInitializeComponents();

	if (IsNetMode(NM_DedicatedServer))
	{
		_physicsControl->DestroyComponent();
		_followCamera->DestroyComponent();
		_cameraBoom->DestroyComponent();
	}
}

void ARunnerCharacter::PostInitializeComponents()
{
	Super::PostInitializeComponents();

	SetupPhysicsControls();
}

void ARunnerCharacter::BeginPlay()
{
	Super::BeginPlay();

	// Bind enhanced input (UEnhancedInputLocalPlayerSubsystem is created too late so we need to try to bind it here)
	BindInput(GetController());

	if (!IsNetMode(NM_DedicatedServer))
	{
		if (_physicsControl != nullptr)
		{
			_physicsControl->SetAllBodyModifierMovementType(_allBodyModifiers.Names, EPhysicsMovementType::Simulated);
			_physicsControl->SetAllControlsEnabled(_skAllControls.Names, true);
			GetMesh()->SetConstraintProfileForAll(FName("HingesOnly"));
			_physicsControl->SetAllBodyModifierGravityMultipliers(_allBodyModifiers.Names, 0.0f);
			_physicsControl->SetBodyModifierMovementType(FName("foot_l_0"), EPhysicsMovementType::Kinematic);
			_physicsControl->SetBodyModifierMovementType(FName("foot_r_0"), EPhysicsMovementType::Kinematic);
		}
	}
}

void ARunnerCharacter::Tick(float deltaSeconds)
{
	Super::Tick(deltaSeconds);

	//BindInput(GetController());

	if (!IsLocallyControlled())
	{
		// Autonomous proxy on owned client looks broken without this
		GetMesh()->SetRelativeTransform(FTransform(FRotator(0.0f, 270.0f, 0.0f), FVector(0.0f, 0.0f, -90.0f),
		                                           FVector::OneVector), false, nullptr, ETeleportType::ResetPhysics);
	}
}

void ARunnerCharacter::PossessedBy(AController* newController)
{
	Super::PossessedBy(newController);

	//Add a mapping context on Possessed
	BindInput(newController);
}

void ARunnerCharacter::UnPossessed()
{
	Super::UnPossessed();

	UnbindInput();
}

void ARunnerCharacter::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
	GetWorldTimerManager().ClearAllTimersForObject(this);

	Super::EndPlay(EndPlayReason);
}

void ARunnerCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	// Set up action bindings
	if (UEnhancedInputComponent* EnhancedInputComponent = CastChecked<UEnhancedInputComponent>(PlayerInputComponent))
	{
		//Jumping
		EnhancedInputComponent->BindAction(_jumpAction, ETriggerEvent::Triggered, this, &ACharacter::Jump);
		EnhancedInputComponent->BindAction(_jumpAction, ETriggerEvent::Completed, this, &ACharacter::StopJumping);

		//Moving
		EnhancedInputComponent->BindAction(_moveAction, ETriggerEvent::Triggered, this, &ARunnerCharacter::Move);

		//Looking
		EnhancedInputComponent->BindAction(_lookAction, ETriggerEvent::Triggered, this, &ARunnerCharacter::Look);
	}
}

void ARunnerCharacter::Move(const FInputActionValue& value)
{
	const FVector2D movementVector = value.Get<FVector2D>();

	if (Controller != nullptr)
	{
		// find out which way is forward
		const FRotator rotation = Controller->GetControlRotation();
		const FRotator yawRotation(0, rotation.Yaw, 0);

		// get forward vector
		const FVector forwardDirection = FRotationMatrix(yawRotation).GetUnitAxis(EAxis::X);

		// get right vector 
		const FVector rightDirection = FRotationMatrix(yawRotation).GetUnitAxis(EAxis::Y);
		
		// add movement 
		AddMovementInput(forwardDirection, movementVector.Y);
		AddMovementInput(rightDirection, movementVector.X);
	}
}

void ARunnerCharacter::Look(const FInputActionValue& Value)
{
	const FVector2D LookAxisVector = Value.Get<FVector2D>();

	if (Controller != nullptr)
	{
		// add yaw and pitch input to controller
		AddControllerYawInput(LookAxisVector.X);
		AddControllerPitchInput(LookAxisVector.Y);
	}
}

void ARunnerCharacter::BindInput(const AController* newController) const
{
	if (!IsLocallyControlled())
	{
		return;
	}

	if (const APlayerController* playerController = Cast<APlayerController>(newController))
	{
		if (UEnhancedInputLocalPlayerSubsystem* subsystem = ULocalPlayer::GetSubsystem<
			UEnhancedInputLocalPlayerSubsystem>(playerController->GetLocalPlayer()))
		{
			subsystem->AddMappingContext(_defaultMappingContext, 0);
		}
	}
}

void ARunnerCharacter::UnbindInput() const
{
	if (const APlayerController* playerController = Cast<APlayerController>(GetController()))
	{
		if (UEnhancedInputLocalPlayerSubsystem* subsystem = ULocalPlayer::GetSubsystem<
			UEnhancedInputLocalPlayerSubsystem>(playerController->GetLocalPlayer()))
		{
			subsystem->RemoveMappingContext(_defaultMappingContext);
		}
	}
}

void ARunnerCharacter::SetupPhysicsControls()
{
	if (_physicsControl == nullptr)
	{
		return;
	}

	GetMesh()->SetConstraintProfileForAll(FName("HingesOnly"));

	const TMap<FName, FPhysicsControlLimbBones> limbBones = _physicsControl->GetLimbBonesFromSkeletalMesh(
		GetMesh(), _limbSetupData);

	FPhysicsControlData physicsControlDataSK;
	physicsControlDataSK.LinearStrength = 3.0f;
	physicsControlDataSK.AngularStrength = 3.0f;
	_skLimbControls = _physicsControl->MakeControlsFromLimbBones(
		_skAllControls, limbBones,
		EPhysicsControlType::WorldSpace, physicsControlDataSK, FPhysicsControlSettings(), false);

	FPhysicsControlData physicsControlDataAA;
	physicsControlDataAA.LinearStrength = 0.0f;
	physicsControlDataAA.AngularStrength = 10.0f;
	_aaLimbControls = _physicsControl->MakeControlsFromLimbBones(
		_aaAllControls, limbBones,
		EPhysicsControlType::ParentSpace, physicsControlDataAA, FPhysicsControlSettings(), false);

	_limbBodyModifiers = _physicsControl->MakeBodyModifiersFromLimbBones(_allBodyModifiers,
	                                                                     limbBones, EPhysicsMovementType::Static,
	                                                                     ECollisionEnabled::QueryAndPhysics, 0.0f,
	                                                                     true);
}

void ARunnerCharacter::NetMulticast_Ragdoll_Implementation()
{
	if (_physicsControl == nullptr)
	{
		return;
	}

	_isStunned = true;

	if (APlayerController* playerController = Cast<APlayerController>(GetController()))
	{
		playerController->SetIgnoreMoveInput(true);
	}

	_physicsControl->SetAllControlsEnabled(_skAllControls.Names, false);
	GetMesh()->SetConstraintProfileForAll(FName("None"), true);
	_physicsControl->SetAllBodyModifierGravityMultipliers(_allBodyModifiers.Names, 1.0f);
	_physicsControl->SetBodyModifierMovementType(FName("foot_l_0"), EPhysicsMovementType::Simulated);
	_physicsControl->SetBodyModifierMovementType(FName("foot_r_0"), EPhysicsMovementType::Simulated);
}

void ARunnerCharacter::NetMulticast_RecoverFromRagdoll_Implementation()
{
	if (HasAuthority())
	{
		GetWorldTimerManager().SetTimer(_stunImmunityTimer, this,
		                                &ARunnerCharacter::NetMulticast_RemoveStunImmunity,
		                                _stunImmunityDuration, false);
	}

	_isStunned = false;

	if (APlayerController* playerController = Cast<APlayerController>(GetController()))
	{
		playerController->SetIgnoreMoveInput(false);
	}

	if (IsNetMode(NM_DedicatedServer))
	{
		return;
	}

	SetActorLocation(GetMesh()->GetComponentLocation());
	GetMesh()->SetRelativeLocation(FVector(0.0f, 0.0f, GetMesh()->GetRelativeLocation().Z), false, nullptr,
	                               ETeleportType::ResetPhysics);

	if (_physicsControl == nullptr)
	{
		return;
	}

	_physicsControl->SetAllControlsEnabled(_skAllControls.Names, true);
	GetMesh()->SetConstraintProfileForAll(FName("HingesOnly"));
	_physicsControl->SetAllBodyModifierGravityMultipliers(_allBodyModifiers.Names, 0.0f);
	_physicsControl->SetBodyModifierMovementType(FName("foot_l_0"), EPhysicsMovementType::Kinematic);
	_physicsControl->SetBodyModifierMovementType(FName("foot_r_0"), EPhysicsMovementType::Kinematic);
}

void ARunnerCharacter::NetMulticast_RemoveStunImmunity_Implementation()
{
	_isImmuneToStun = false;
}

void ARunnerCharacter::OnCapsuleBeginOverlap(UPrimitiveComponent* overlappedComponent, AActor* otherActor,
                                             UPrimitiveComponent* otherComp, int otherBodyIndex, bool bFromSweep,
                                             const FHitResult& hitResult)
{
	// todo traps should have this responsibility, not runner
	//StunPlayer(_defaultStunDuration);
	//KillPlayer();
}
