﻿#pragma once
#include "TrapBase.h"
#include "TrapPlatforms.generated.h"

UCLASS()

class ATrapPlatforms : public ATrapBase
{
	GENERATED_BODY()
public:
	ATrapPlatforms();
	
	virtual void Interact(APlayerController* player, bool isFake) override;

	virtual void Activate() override;

	virtual void FakeActivate() override;

	/*
 * Called before initialization of components, destroys components that are not needed on the server.
 */
	virtual void PreInitializeComponents() override;


protected:
	virtual void BeginPlay() override;

protected:
	
	virtual void NetMulticast_Activation_Implementation() override;

	UPROPERTY(EditDefaultsOnly, Category = "Trap|Gameplay")
	UStaticMeshComponent* _platform1;

	UPROPERTY(EditDefaultsOnly, Category = "Trap|Gameplay")
	UStaticMeshComponent* _platform2;
	
private:

	void SwapPlatforms() const;

};
