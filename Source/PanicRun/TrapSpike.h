﻿#pragma once
#include "TrapBase.h"
#include "TrapSpike.generated.h"

UCLASS()

class ATrapSpike : public ATrapBase
{
	GENERATED_BODY()
public:
	
	ATrapSpike();
	
	virtual void Interact(APlayerController* player, bool isFake) override;

	virtual void Activate() override;

	virtual void FakeActivate() override;

	/*
 * Called before initialization of components, destroys components that are not needed on the server.
 */
	virtual void PreInitializeComponents() override;

	// AActor

protected:
	virtual void BeginPlay() override;

private:

	void RepeatingFunction();

	/* Handle to manage the timer */
	FTimerHandle _memberTimerHandle;

};
