#pragma once

#include "CoreMinimal.h"
#include "APanicRunPlayerController.h"
#include "WardenCharacter.h"
#include "GameFramework/GameMode.h"
#include "PanicRunGameMode.generated.h"

class ATrapBase;

UCLASS(minimalapi)
class APanicRunGameMode : public AGameMode
{
	GENERATED_BODY()

public:
	APanicRunGameMode();

	void PlayerDied(APanicRunPlayerController* playerController);

	void RunnerReachedFinish();

	// AGameMode
public:
	virtual void BeginPlay() override;
	
	/*
	 * Determines player pawn class - runner or warden.
	 */
	virtual UClass* GetDefaultPawnClassForController_Implementation(AController* inController) override;

	/*
	 * Determines player start location - warden spawns at a different location than runners.
	 */
	virtual AActor* FindPlayerStart_Implementation(AController* player, const FString& incomingName) override;

	virtual void PostLogin(APlayerController* NewPlayer) override;

	virtual void Logout(AController* exiting) override;

	virtual bool ReadyToStartMatch_Implementation() override;

	virtual void StartMatch() override;

	virtual bool ReadyToEndMatch_Implementation() override;

protected:
	/*
	 * Procedurally spawns traps in the level.
	 * Also spawns runner finish line.
	 */
	virtual void GenerateLevel();

	/*
	 * Choose a random player to be the warden.
	 */
	virtual void ChooseWarden();

private:
	/*
	 * Warden class that will be spawned at the start of the game.
	 */
	UPROPERTY(EditDefaultsOnly, Category = "PanicRun")
	TSubclassOf<AWardenCharacter> _wardenPawnClass;

	/*
	 * List of trap classes that will be spawned in the level procedurally.
	 */
	UPROPERTY(EditDefaultsOnly, Category = "PanicRun")
	TArray<TSubclassOf<ATrapBase>> _trapClasses;

	/*
	 * Finish class that will be instanced at the end of the level.
	 * Grants the runner a win condition.
	 */
	UPROPERTY(EditDefaultsOnly, Category = "PanicRun")
	TSubclassOf<AActor> _finishClass;

	/*
	 * Minimum number of players required to start the match.
	 * Includes a warden.
	 */
	UPROPERTY(EditDefaultsOnly, Category = "PanicRun")
	int32 _minNumberOfPlayers = 3;

	/*
	 * Number of traps that will be spawned in the level procedurally.
	 */
	UPROPERTY(EditDefaultsOnly, Category = "PanicRun")
	int32 _numberOfTraps = 10;

	/*
	 * Match duration in minutes.
	 */
	UPROPERTY(EditDefaultsOnly, Category = "PanicRun")
	float _matchDuration = 10.0f;

	UPROPERTY()
	APlayerController* _warden;

	FTimerHandle _waitMatchStartTimer;

	bool _waitedForPlayers = false;
	bool _currentlyWaitingForPlayers = false;

	UPROPERTY(EditDefaultsOnly, Category = "PanicRun")
	float _waitingForPlayersTime = 15.0f;

private:
	void DeclareWardenVictory();
	void DeclareRunnersVictory();

	UFUNCTION()
	void OnWaitForPlayersElapsed();
};
