﻿#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "PauseMenuWidget.generated.h"

class UWidgetSwitcher;
class UButton;

UCLASS()
class PANICRUN_API UPauseMenuWidget : public UUserWidget
{
	GENERATED_BODY()

public:
	virtual void NativeConstruct() override;

	virtual void SetVisibility(ESlateVisibility inVisibility) override;
	
private:
	UPROPERTY(Meta = (BindWidget))
	UButton* _resumeButton;

	UPROPERTY(Meta = (BindWidget))
	UButton* _settingsButton;
	
	UPROPERTY(Meta = (BindWidget))
	UButton* _quitToMainMenuButton;

	UPROPERTY(Meta = (BindWidget))
	UButton* _backButton;

	UPROPERTY(Meta = (BindWidget))
	UWidgetSwitcher* _widgetSwitcher;

private:
	UFUNCTION()
	void Resume();

	UFUNCTION()
	void ShowSettingsWidget();

	UFUNCTION()
	void ShowPauseMenuWidget();

	UFUNCTION()
	void QuitToMainMenu();
};
