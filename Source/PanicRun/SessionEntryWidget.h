﻿#pragma once

#include "OnlineSessionSettings.h"
#include "Blueprint/IUserObjectListEntry.h"
#include "Blueprint/UserWidget.h"
#include "SessionEntryWidget.generated.h"

class UBorder;
UCLASS()
class UOnlineSessionSearchResultWrapper : public UObject
{
	GENERATED_BODY()
public:
	FOnlineSessionSearchResult Session;
};

class UTextBlock;

UCLASS()
class PANICRUN_API USessionEntryWidget : public UUserWidget, public IUserObjectListEntry
{
	GENERATED_BODY()

public:
	virtual void NativeConstruct() override;

	virtual void NativeOnListItemObjectSet(UObject* inObject) override;

	virtual void NativeOnItemSelectionChanged(bool isSelected) override;

private:
	UPROPERTY(Meta = (BindWidget))
	UBorder* _border;
	
	UPROPERTY(Meta = (BindWidget))
	UTextBlock* _sessionNameText;

	UPROPERTY(Meta = (BindWidget))
	UTextBlock* _numPlayersText;

	UPROPERTY(Meta = (BindWidget))
	UTextBlock* _pingText;
};
