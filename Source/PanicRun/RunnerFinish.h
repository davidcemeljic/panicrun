﻿#pragma once

#include "GameFramework/Actor.h"
#include "RunnerFinish.generated.h"

class USceneComponent;
class UStaticMeshComponent;
class UBoxComponent;

UCLASS()
class PANICRUN_API ARunnerFinish : public AActor
{
	GENERATED_BODY()

public:
	ARunnerFinish();

private:
	UPROPERTY(EditDefaultsOnly, Category = "Components")
	USceneComponent* _root;
	
	UPROPERTY(EditDefaultsOnly, Category = "Components")
	UStaticMeshComponent* _mesh;
	
	UPROPERTY(EditDefaultsOnly, Category = "Components")
	UBoxComponent* _boxCollision;

private:
	UFUNCTION()
	void OnBeginOverlap(UPrimitiveComponent* overlappedComponent, AActor* otherActor,
						UPrimitiveComponent* otherComp, int otherBodyIndex, bool fromSweep,
						const FHitResult& sweepResult);
};
