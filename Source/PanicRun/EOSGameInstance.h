﻿#pragma once
#include "OnlineSessionSettings.h"
#include "Interfaces/OnlineSessionInterface.h"

#include "EOSGameInstance.generated.h"

DECLARE_EVENT(UEOSGameInstance, FOnSessionSearchComplete);

UCLASS()
class USessionInfo : public UObject
{
	GENERATED_BODY()

public:
	FString SessionName;
	FString SessionId;
	FString SessionOwner;
	FString SessionOwnerId;
	FOnlineSessionSettings SessionSettings;
	int32 NumPublicConnections;
};

class IOnlineSubsystem;
class FOnlineSessionSearch;

UCLASS()
class PANICRUN_API UEOSGameInstance : public UGameInstance
{
	GENERATED_BODY()

public:
	FOnSessionSearchComplete OnSessionSearchComplete;

public:
	UEOSGameInstance();

	void Login();

	void CreateSession();

	void DestroySession();

	void FindSessions();
	
	virtual bool JoinSession(ULocalPlayer* LocalPlayer, const FOnlineSessionSearchResult& searchResult) override;

	const TArray<FOnlineSessionSearchResult>& GetSessions() const;

	// UGameInstance
public:
	virtual void Init() override;

protected:
	IOnlineSubsystem* OnlineSubsystem;

	bool isLoggedIn = false;

private:
	const FName _sessionName = FName("PanicRunSession");

	TSharedPtr<FOnlineSessionSearch> _sessionSearchSettings;

private:
	void OnLoginComplete(int32 localUserNumber, bool wasSuccessful, const FUniqueNetId& uniqueNetId,
	                     const FString& error);

	void OnCreateSessionComplete(FName sessionName, bool wasSuccessful);

	void OnDestroySessionComplete(FName sessionName, bool bWasSuccessful);

	void OnFindSessionsComplete(bool wasSuccessful);

	void OnJoinSessionComplete(FName sessionName, EOnJoinSessionCompleteResult::Type result);
};
