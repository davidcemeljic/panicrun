﻿#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "KillVolume.generated.h"

class UBoxComponent;

UCLASS()
class PANICRUN_API AKillVolume : public AActor
{
	GENERATED_BODY()

public:
	AKillVolume();

private:
	UPROPERTY(EditAnywhere, Category = "Components")
	UBoxComponent* _boxCollision;

private:
	UFUNCTION()
	void OnOverlapBegin(UPrimitiveComponent* overlappedComponent, AActor* otherActor,
	                    UPrimitiveComponent* otherComp, int otherBodyIndex, bool fromSweep,
	                    const FHitResult& sweepResult);
};
