﻿#pragma once
#include "Blueprint/UserWidget.h"

#include "SessionBrowserWidget.generated.h"

class UListView;
class UButton;

UCLASS()
class PANICRUN_API USessionBrowserWidget : public UUserWidget
{
	GENERATED_BODY()

public:
	virtual void NativeConstruct() override;

	UFUNCTION()
	void Refresh();

private:
	UPROPERTY(Meta = (BindWidget))
	UButton* _createButton;

	UPROPERTY(Meta = (BindWidget))
	UButton* _refreshButton;

	UPROPERTY(Meta = (BindWidget))
	UButton* _joinButton;

	UPROPERTY(Meta = (BindWidget))
	UListView* _sessionList;

private:
	UFUNCTION()
	void Create();

	UFUNCTION()
	void Join();

	void UpdateSessionList() const;
};
