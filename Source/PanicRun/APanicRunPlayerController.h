﻿#pragma once

#include "CoreMinimal.h"
#include "InputActionValue.h"
#include "APanicRunPlayerController.generated.h"

class UInputAction;
class UInputMappingContext;
class IInteractable;

UCLASS()
class PANICRUN_API APanicRunPlayerController : public APlayerController

{
	GENERATED_BODY()

public:
	APanicRunPlayerController();

	FTimerHandle& GetRespawnTimer()
	{
		return _respawnTimer;
	}

	void RespawnTimerStarted(float respawnTime);
	void Interact(UObject* interactable, APlayerController* playerController, bool isFake);

public:
	virtual void SetupInputComponent() override;

private:
	UPROPERTY(EditDefaultsOnly, Category = "PanicRun|Input")
	UInputMappingContext* _inputMappingContext;

	UPROPERTY(EditDefaultsOnly, Category = "PanicRun|Input")
	UInputAction* _openPauseMenuAction;
	
	FTimerHandle _respawnTimer;
	
private:

	UFUNCTION(Client, Reliable)
	void Client_RespawnTimerStarted(float respawnTime);

	UFUNCTION(Server, Reliable)
	void Server_Interact(UObject* interactable, APlayerController* playerController, bool isFake);
	
	void TogglePauseMenu(const FInputActionValue& inputActionValue);
};
