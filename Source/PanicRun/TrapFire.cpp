﻿#include "TrapFire.h"

#include "RunnerCharacter.h"
#include "Kismet/GameplayStatics.h"
#include "Particles/ParticleSystemComponent.h"


ATrapFire::ATrapFire()
{
	_killRange->OnComponentBeginOverlap.AddDynamic(this, &ATrapFire::OnCollision);
}

void ATrapFire::Interact(APlayerController* player, bool isFake)
{
	Super::Interact(player, isFake);
}

void ATrapFire::Activate()
{
	if (!HasAuthority())
	{
		return;
	}
	//Get overlapping actors and kill all runners
	
	UE_LOG(LogTemp, Log, TEXT("Interact Called in trap"));
	TArray<AActor*> OverlappingActors;
	_killRange->GetOverlappingActors(OverlappingActors);
	for (AActor* Actor : OverlappingActors)
	{
		//UE_LOG(LogTemp, Log, TEXT("Tested overlapping actor"));
		if (ARunnerCharacter* RunnerCharacter = Cast<ARunnerCharacter>(Actor))
		{
			//UE_LOG(LogTemp, Log, TEXT("Found runner"));
			RunnerCharacter->KillPlayer();
		}
	}
}

void ATrapFire::FakeActivate()
{
	if (!HasAuthority())
	{
		return;
	}
	//unimplemented();
	UE_LOG(LogTemp, Log, TEXT("Fake activation!"));
}

void ATrapFire::PreInitializeComponents()
{
	Super::PreInitializeComponents();
}

void ATrapFire::BeginPlay()
{
	Super::BeginPlay();
}

void ATrapFire::NetMulticast_Activation_Implementation()
{
	Super::NetMulticast_Activation_Implementation();
	_isActive = true;
	if(_PSfire)
	{
		_PSystem = UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), _PSfire, _trapMesh->GetComponentLocation());
		GetWorld()->GetTimerManager().SetTimer(_PStimerHandle, this, &ATrapFire::DestroyPS, _cooldown, false);
	}
}

//function that destroys the particle system
void ATrapFire::DestroyPS()
{
	_isActive = false;
	_PSystem->DestroyComponent();
}

void ATrapFire::OnCollision(UPrimitiveComponent* OverlappedComponent, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if(_isActive)
	{
		if (ARunnerCharacter* RunnerCharacter = Cast<ARunnerCharacter>(OtherActor))
		{
			RunnerCharacter->KillPlayer();
		}
	}
}
