#include "PanicRunGameMode.h"
#include "APanicRunPlayerController.h"
#include "EOSGameInstance.h"
#include "PanicRunGameState.h"
#include "PanicRunHUD.h"
#include "RunnerCharacter.h"
#include "TrapBase.h"
#include "GameFramework/GameState.h"
#include "Kismet/GameplayStatics.h"

APanicRunGameMode::APanicRunGameMode()
{
	GameStateClass = APanicRunGameState::StaticClass();
	DefaultPawnClass = ARunnerCharacter::StaticClass();
	PlayerControllerClass = APanicRunPlayerController::StaticClass();
	HUDClass = APanicRunHUD::StaticClass();
}

void APanicRunGameMode::PlayerDied(APanicRunPlayerController* playerController)
{
	if (playerController == nullptr)
	{
		UE_LOG(LogTemp, Error, TEXT("APanicRunGameMode::PlayerDied - PlayerController is null!"));
		return;
	}

	const TWeakObjectPtr<APanicRunGameMode> weakThis = this;
	const TWeakObjectPtr<APanicRunPlayerController> weakPlayerController = playerController;
	const FTimerDelegate respawnDelegate = FTimerDelegate::CreateLambda([weakThis, weakPlayerController]()
	{
		if (!weakThis.IsValid() || !weakPlayerController.IsValid())
		{
			return;
		}

		weakThis->RestartPlayer(weakPlayerController.Get());
		if (ARunnerCharacter* runnerCharacter = Cast<ARunnerCharacter>(weakPlayerController->GetPawn()))
		{
			runnerCharacter->Reset();

			if (const AActor* startSpot = weakThis->FindPlayerStart_Implementation(
				weakPlayerController.Get(), FString()))
			{
				runnerCharacter->SetActorLocation(startSpot->GetActorLocation());
			}
		}
	});

	const float respawnTime = MinRespawnDelay;
	GetWorldTimerManager().SetTimer(playerController->GetRespawnTimer(), respawnDelegate, respawnTime, false);
	playerController->RespawnTimerStarted(respawnTime);
}

void APanicRunGameMode::RunnerReachedFinish()
{
	DeclareRunnersVictory();
}

void APanicRunGameMode::BeginPlay()
{
	Super::BeginPlay();

	GenerateLevel();
}

UClass* APanicRunGameMode::GetDefaultPawnClassForController_Implementation(AController* inController)
{
	if (inController == _warden && _wardenPawnClass != nullptr)
	{
		return _wardenPawnClass;
	}

	return Super::GetDefaultPawnClassForController_Implementation(inController);
}

AActor* APanicRunGameMode::FindPlayerStart_Implementation(AController* player, const FString& incomingName)
{
	if (player == _warden)
	{
		return Super::FindPlayerStart_Implementation(player, TEXT("WardenStart"));
	}

	return Super::FindPlayerStart_Implementation(player, incomingName);
}

void APanicRunGameMode::PostLogin(APlayerController* NewPlayer)
{
	Super::PostLogin(NewPlayer);
}

void APanicRunGameMode::Logout(AController* exiting)
{
	if (exiting == UGameplayStatics::GetPlayerController(this, 0))
	{
		if (UEOSGameInstance* gameInstance = Cast<UEOSGameInstance>(GetGameInstance()))
		{
			gameInstance->DestroySession();
		}
	}

	Super::Logout(exiting);
}

bool APanicRunGameMode::ReadyToStartMatch_Implementation()
{
	if (GameState->PlayerArray.Num() >= _minNumberOfPlayers && Super::ReadyToStartMatch_Implementation() && !
		_currentlyWaitingForPlayers)
	{
		_currentlyWaitingForPlayers = true;
		GetWorldTimerManager().SetTimer(_waitMatchStartTimer, this, &APanicRunGameMode::OnWaitForPlayersElapsed,
		                                _waitingForPlayersTime, false);
		return false;
	}

	return _waitedForPlayers;
}

void APanicRunGameMode::StartMatch()
{
	ChooseWarden();

	if (APanicRunGameState* gameState = GetGameState<APanicRunGameState>())
	{
		gameState->OnMatchTimerExpired.AddUObject(this, &APanicRunGameMode::DeclareWardenVictory);
		gameState->StartMatchTimer(_matchDuration);
	}
	else
	{
		UE_LOG(LogTemp, Error, TEXT("APanicRunGameMode::StartMatch - Game State is nullptr!"));
	}

	Super::StartMatch();
}

bool APanicRunGameMode::ReadyToEndMatch_Implementation()
{
	return false;
}

void APanicRunGameMode::GenerateLevel()
{
	UE_LOG(LogTemp, Log, TEXT("Generating level"));

	FVector boxExtent, origin;
	float offset = 0;

	for (int32 i = 0; i < _numberOfTraps; ++i)
	{
		const int32 index = FMath::RandRange(0, _trapClasses.Num() - 1);
		const TSubclassOf<ATrapBase> trapClass = _trapClasses[index];
		if (trapClass == nullptr)
		{
			UE_LOG(LogTemp, Error, TEXT("APanicRunGameMode::GenerateLevel - Trap class is nullptr!"));
			continue;
		}

		const ATrapBase* newTrap = GetWorld()->SpawnActor<ATrapBase>(
			_trapClasses[index],
			FVector::ForwardVector * offset,
			FRotator(0.0f, 90.0f, 0.0f));

		if (newTrap != nullptr)
		{
			newTrap->GetActorBounds(false, origin, boxExtent);
			offset += boxExtent.X * 2.0f;
		}
	}

	if (_finishClass != nullptr)
	{
		GetWorld()->SpawnActor<AActor>(
			_finishClass,
			FVector::ForwardVector * offset,
			FRotator::ZeroRotator);
	}
}

void APanicRunGameMode::ChooseWarden()
{
	TArray<AActor*> playerControllers;
	UGameplayStatics::GetAllActorsOfClass(GetWorld(), APlayerController::StaticClass(), playerControllers);

	if (playerControllers.IsEmpty())
	{
		UE_LOG(LogTemp, Error, TEXT("APanicRunGameMode::ChooseWarden - No player controllers found!"));
		return;
	}

	const int32 index = FMath::RandRange(0, playerControllers.Num() - 1);

	if (APlayerController* playerController = Cast<APlayerController>(playerControllers[index]))
	{
		_warden = playerController;
	}
}

void APanicRunGameMode::DeclareRunnersVictory()
{
	if (GetMatchState() != MatchState::InProgress)
	{
		return;
	}

	if (APanicRunGameState* gameState = GetGameState<APanicRunGameState>())
	{
		gameState->RunnersWon();
	}

	EndMatch();
}

void APanicRunGameMode::OnWaitForPlayersElapsed()
{
	_waitedForPlayers = true;
}

void APanicRunGameMode::DeclareWardenVictory()
{
	if (GetMatchState() != MatchState::InProgress)
	{
		return;
	}

	if (APanicRunGameState* gameState = GetGameState<APanicRunGameState>())
	{
		gameState->WardenWon();
	}

	EndMatch();
}
